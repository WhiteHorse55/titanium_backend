-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 24, 2018 at 06:28 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_dine`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `username`, `password`, `created`) VALUES
(1, 'aa', 'e32498fb233396b17249bfacd267df3eb8fe1308', '2018-05-23'),
(2, 'bb', 'e32498fb233396b17249bfacd267df3eb8fe1308', '2018-05-23'),
(3, 'dfgdf', '130415edeeba157ffcb5eae0700384a0ff30a846', '2018-05-23');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_base`
--

CREATE TABLE `tbl_base` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(1024) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_base`
--

INSERT INTO `tbl_base` (`id`, `name`) VALUES
(3, 'ytyreyrey'),
(4, 'fdhdfghgdhf');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_booking`
--

CREATE TABLE `tbl_booking` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `plates` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_country`
--

CREATE TABLE `tbl_country` (
  `id` int(10) UNSIGNED NOT NULL,
  `short` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_country`
--

INSERT INTO `tbl_country` (`id`, `short`, `name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'AS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AR', 'Argentina'),
(10, 'AM', 'Armenia'),
(11, 'AW', 'Aruba'),
(12, 'AU', 'Australia'),
(13, 'AT', 'Austria'),
(14, 'AZ', 'Azerbaijan'),
(15, 'BS', 'Bahamas'),
(16, 'BH', 'Bahrain'),
(17, 'BD', 'Bangladesh'),
(18, 'BB', 'Barbados'),
(19, 'BY', 'Belarus'),
(20, 'BE', 'Belgium'),
(21, 'BZ', 'Belize'),
(22, 'BJ', 'Benin'),
(23, 'BM', 'Bermuda'),
(24, 'BT', 'Bhutan'),
(25, 'BO', 'Bolivia'),
(26, 'BA', 'Bosnia and Herzegowina'),
(27, 'BW', 'Botswana'),
(28, 'BV', 'Bouvet Island'),
(29, 'BR', 'Brazil'),
(30, 'IO', 'British Indian Ocean Territory'),
(31, 'BN', 'Brunei Darussalam'),
(32, 'BG', 'Bulgaria'),
(33, 'BF', 'Burkina Faso'),
(34, 'BI', 'Burundi'),
(35, 'KH', 'Cambodia'),
(36, 'CM', 'Cameroon'),
(37, 'CA', 'Canada'),
(38, 'CV', 'Cape Verde'),
(39, 'KY', 'Cayman Islands'),
(40, 'CF', 'Central African Republic'),
(41, 'TD', 'Chad'),
(42, 'CL', 'Chile'),
(43, 'CN', 'China'),
(44, 'CX', 'Christmas Island'),
(45, 'CC', 'Cocos (Keeling) Islands'),
(46, 'CO', 'Colombia'),
(47, 'KM', 'Comoros'),
(48, 'CG', 'Congo'),
(49, 'CD', 'Congo, the Democratic Republic of the'),
(50, 'CK', 'Cook Islands'),
(51, 'CR', 'Costa Rica'),
(52, 'CI', 'Cote d\'Ivoire'),
(53, 'HR', 'Croatia (Hrvatska)'),
(54, 'CU', 'Cuba'),
(55, 'CY', 'Cyprus'),
(56, 'CZ', 'Czech Republic'),
(57, 'DK', 'Denmark'),
(58, 'DJ', 'Djibouti'),
(59, 'DM', 'Dominica'),
(60, 'DO', 'Dominican Republic'),
(61, 'EC', 'Ecuador'),
(62, 'EG', 'Egypt'),
(63, 'SV', 'El Salvador'),
(64, 'GQ', 'Equatorial Guinea'),
(65, 'ER', 'Eritrea'),
(66, 'EE', 'Estonia'),
(67, 'ET', 'Ethiopia'),
(68, 'FK', 'Falkland Islands (Malvinas)'),
(69, 'FO', 'Faroe Islands'),
(70, 'FJ', 'Fiji'),
(71, 'FI', 'Finland'),
(72, 'FR', 'France'),
(73, 'GF', 'French Guiana'),
(74, 'PF', 'French Polynesia'),
(75, 'TF', 'French Southern Territories'),
(76, 'GA', 'Gabon'),
(77, 'GM', 'Gambia'),
(78, 'GE', 'Georgia'),
(79, 'DE', 'Germany'),
(80, 'GH', 'Ghana'),
(81, 'GI', 'Gibraltar'),
(82, 'GR', 'Greece'),
(83, 'GL', 'Greenland'),
(84, 'GD', 'Grenada'),
(85, 'GP', 'Guadeloupe'),
(86, 'GU', 'Guam'),
(87, 'GT', 'Guatemala'),
(88, 'GN', 'Guinea'),
(89, 'GW', 'Guinea-Bissau'),
(90, 'GY', 'Guyana'),
(91, 'HT', 'Haiti'),
(92, 'HM', 'Heard and Mc Donald Islands'),
(93, 'VA', 'Holy See (Vatican City State)'),
(94, 'HN', 'Honduras'),
(95, 'HK', 'Hong Kong'),
(96, 'HU', 'Hungary'),
(97, 'IS', 'Iceland'),
(98, 'IN', 'India'),
(99, 'ID', 'Indonesia'),
(100, 'IR', 'Iran (Islamic Republic of)'),
(101, 'IQ', 'Iraq'),
(102, 'IE', 'Ireland'),
(103, 'IL', 'Israel'),
(104, 'IT', 'Italy'),
(105, 'JM', 'Jamaica'),
(106, 'JP', 'Japan'),
(107, 'JO', 'Jordan'),
(108, 'KZ', 'Kazakhstan'),
(109, 'KE', 'Kenya'),
(110, 'KI', 'Kiribati'),
(111, 'KP', 'Korea, Democratic People\'s Republic of'),
(112, 'KR', 'Korea, Republic of'),
(113, 'KW', 'Kuwait'),
(114, 'KG', 'Kyrgyzstan'),
(115, 'LA', 'Lao People\'s Democratic Republic'),
(116, 'LV', 'Latvia'),
(117, 'LB', 'Lebanon'),
(118, 'LS', 'Lesotho'),
(119, 'LR', 'Liberia'),
(120, 'LY', 'Libyan Arab Jamahiriya'),
(121, 'LI', 'Liechtenstein'),
(122, 'LT', 'Lithuania'),
(123, 'LU', 'Luxembourg'),
(124, 'MO', 'Macau'),
(125, 'MK', 'Macedonia, The Former Yugoslav Republic of'),
(126, 'MG', 'Madagascar'),
(127, 'MW', 'Malawi'),
(128, 'MY', 'Malaysia'),
(129, 'MV', 'Maldives'),
(130, 'ML', 'Mali'),
(131, 'MT', 'Malta'),
(132, 'MH', 'Marshall Islands'),
(133, 'MQ', 'Martinique'),
(134, 'MR', 'Mauritania'),
(135, 'MU', 'Mauritius'),
(136, 'YT', 'Mayotte'),
(137, 'MX', 'Mexico'),
(138, 'FM', 'Micronesia, Federated States of'),
(139, 'MD', 'Moldova, Republic of'),
(140, 'MC', 'Monaco'),
(141, 'MN', 'Mongolia'),
(142, 'MS', 'Montserrat'),
(143, 'MA', 'Morocco'),
(144, 'MZ', 'Mozambique'),
(145, 'MM', 'Myanmar'),
(146, 'NA', 'Namibia'),
(147, 'NR', 'Nauru'),
(148, 'NP', 'Nepal'),
(149, 'NL', 'Netherlands'),
(150, 'AN', 'Netherlands Antilles'),
(151, 'NC', 'New Caledonia'),
(152, 'NZ', 'New Zealand'),
(153, 'NI', 'Nicaragua'),
(154, 'NE', 'Niger'),
(155, 'NG', 'Nigeria'),
(156, 'NU', 'Niue'),
(157, 'NF', 'Norfolk Island'),
(158, 'MP', 'Northern Mariana Islands'),
(159, 'NO', 'Norway'),
(160, 'OM', 'Oman'),
(161, 'PK', 'Pakistan'),
(162, 'PW', 'Palau'),
(163, 'PA', 'Panama'),
(164, 'PG', 'Papua New Guinea'),
(165, 'PY', 'Paraguay'),
(166, 'PE', 'Peru'),
(167, 'PH', 'Philippines'),
(168, 'PN', 'Pitcairn'),
(169, 'PL', 'Poland'),
(170, 'PT', 'Portugal'),
(171, 'PR', 'Puerto Rico'),
(172, 'QA', 'Qatar'),
(173, 'RE', 'Reunion'),
(174, 'RO', 'Romania'),
(175, 'RU', 'Russian Federation'),
(176, 'RW', 'Rwanda'),
(177, 'KN', 'Saint Kitts and Nevis'),
(178, 'LC', 'Saint LUCIA'),
(179, 'VC', 'Saint Vincent and the Grenadines'),
(180, 'WS', 'Samoa'),
(181, 'SM', 'San Marino'),
(182, 'ST', 'Sao Tome and Principe'),
(183, 'SA', 'Saudi Arabia'),
(184, 'SN', 'Senegal'),
(185, 'SC', 'Seychelles'),
(186, 'SL', 'Sierra Leone'),
(187, 'SG', 'Singapore'),
(188, 'SK', 'Slovakia (Slovak Republic)'),
(189, 'SI', 'Slovenia'),
(190, 'SB', 'Solomon Islands'),
(191, 'SO', 'Somalia'),
(192, 'ZA', 'South Africa'),
(193, 'GS', 'South Georgia and the South Sandwich Islands'),
(194, 'ES', 'Spain'),
(195, 'LK', 'Sri Lanka'),
(196, 'SH', 'St. Helena'),
(197, 'PM', 'St. Pierre and Miquelon'),
(198, 'SD', 'Sudan'),
(199, 'SR', 'Suriname'),
(200, 'SJ', 'Svalbard and Jan Mayen Islands'),
(201, 'SZ', 'Swaziland'),
(202, 'SE', 'Sweden'),
(203, 'CH', 'Switzerland'),
(204, 'SY', 'Syrian Arab Republic'),
(205, 'TW', 'Taiwan, Province of China'),
(206, 'TJ', 'Tajikistan'),
(207, 'TZ', 'Tanzania, United Republic of'),
(208, 'TH', 'Thailand'),
(209, 'TG', 'Togo'),
(210, 'TK', 'Tokelau'),
(211, 'TO', 'Tonga'),
(212, 'TT', 'Trinidad and Tobago'),
(213, 'TN', 'Tunisia'),
(214, 'TR', 'Turkey'),
(215, 'TM', 'Turkmenistan'),
(216, 'TC', 'Turks and Caicos Islands'),
(217, 'TV', 'Tuvalu'),
(218, 'UG', 'Uganda'),
(219, 'UA', 'Ukraine'),
(220, 'AE', 'United Arab Emirates'),
(221, 'GB', 'United Kingdom'),
(222, 'US', 'United States'),
(223, 'UM', 'United States Minor Outlying Islands'),
(224, 'UY', 'Uruguay'),
(225, 'UZ', 'Uzbekistan'),
(226, 'VU', 'Vanuatu'),
(227, 'VE', 'Venezuela'),
(228, 'VN', 'Viet Nam'),
(229, 'VG', 'Virgin Islands (British)'),
(230, 'VI', 'Virgin Islands (U.S.)'),
(231, 'WF', 'Wallis and Futuna Islands'),
(232, 'EH', 'Western Sahara'),
(233, 'YE', 'Yemen'),
(234, 'ZM', 'Zambia'),
(235, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `image_url` text COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `style_id` int(11) NOT NULL,
  `base_id` int(11) NOT NULL,
  `other_id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `max_takeaway` int(11) NOT NULL,
  `max_dine` int(11) NOT NULL,
  `open_time` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `close_time` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `serve_time` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `menu_day` varchar(1024) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu_gallery`
--

CREATE TABLE `tbl_menu_gallery` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_other`
--

CREATE TABLE `tbl_other` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(1024) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_other`
--

INSERT INTO `tbl_other` (`id`, `name`) VALUES
(1, 'asdfasf');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment`
--

CREATE TABLE `tbl_payment` (
  `id` int(10) UNSIGNED NOT NULL,
  `card_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_restaurant`
--

CREATE TABLE `tbl_restaurant` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `welcome_message` text COLLATE utf8_unicode_ci NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_restaurant`
--

INSERT INTO `tbl_restaurant` (`id`, `name`, `user_id`, `welcome_message`, `lat`, `lng`, `address`, `created`) VALUES
(5, 'Bakery', 2, 'dsfassfdassdfdsfadfadsdf', 23.77556, 324.3425435234523, 'aaaaa', '2018-05-24 09:05:48');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_style`
--

CREATE TABLE `tbl_style` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(1024) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_style`
--

INSERT INTO `tbl_style` (`id`, `name`) VALUES
(4, 'aaa'),
(6, 'dddd'),
(7, 'sdfdsf'),
(8, 'eeeee'),
(9, 'fff'),
(10, 'dfs'),
(12, 'rewrw'),
(14, 'gfgerwr'),
(15, 'bbb');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `first_name` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(30) DEFAULT NULL,
  `email` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `profile` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` enum('chef','diner') COLLATE utf8_unicode_ci NOT NULL,
  `payment_id` int(11) NOT NULL,
  `created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `first_name`, `last_name`, `phone`, `email`, `password`, `profile`, `user_type`, `payment_id`, `created`) VALUES
(1, 'Doe', 'John', 133908091, 'aa@aa.com', 'e32498fb233396b17249bfacd267df3eb8fe1308', NULL, 'chef', 1, '2018-05-16'),
(2, 'Michele', 'Jordan', 1908928372, 'bb@bb.com', 'e32498fb233396b17249bfacd267df3eb8fe1308', NULL, 'diner', 8, '2018-05-16'),
(3, 'Keith', 'Sarah', 123456987, 'cc@cc.com', 'e32498fb233396b17249bfacd267df3eb8fe1308', NULL, 'chef', 2, '2018-05-02'),
(4, 'Paul', 'Keita', 123456789, 'dd@dd.com', 'e32498fb233396b17249bfacd267df3eb8fe1308', NULL, 'chef', 4, '2018-05-02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_base`
--
ALTER TABLE `tbl_base`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_booking`
--
ALTER TABLE `tbl_booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_country`
--
ALTER TABLE `tbl_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_menu_gallery`
--
ALTER TABLE `tbl_menu_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_other`
--
ALTER TABLE `tbl_other`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_restaurant`
--
ALTER TABLE `tbl_restaurant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_style`
--
ALTER TABLE `tbl_style`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_base`
--
ALTER TABLE `tbl_base`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_booking`
--
ALTER TABLE `tbl_booking`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_country`
--
ALTER TABLE `tbl_country`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=236;

--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_menu_gallery`
--
ALTER TABLE `tbl_menu_gallery`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_other`
--
ALTER TABLE `tbl_other`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_restaurant`
--
ALTER TABLE `tbl_restaurant`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_style`
--
ALTER TABLE `tbl_style`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
