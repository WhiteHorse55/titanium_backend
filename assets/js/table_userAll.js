$(document).ready(function() {

	initDataTable();

	function initDataTable(){
		var datatable = $('#clients-all-table').DataTable({
						"autoWidth": true,
				        "width": '100%',
	                    "processing": true,
	                    "serverSide": true,
	                    "scrollX": true,
	                    "fixedColumns":   {
				            leftColumns: 1,
				        },
				        // "deferRender": true,
	                    "ajax": {
	                        url:  `${base_url}user/getSearchData`,
	                        type: "POST",
	                        "dataSrc": function (json) {
	                        	console.log(json);
	                            var return_data = new Array();
	                            for(var i=0; i< json.data.length; i++){
	                                return_data.push({
									'id': json.data[i].id,
	                                'email'  : json.data[i].email,
	                                'password'  : json.data[i].password,
	                                'firstname'  : json.data[i].firstname,
	                                'lastname'  : json.data[i].lastname,
	                                'phonenumber'  : json.data[i].phonenumber,
	                                'timestamp'  : json.data[i].timestamp,
	                                });
	                            }
	                            return return_data;
	                        }
	                    },

	                    "order": [[0,'desc']],
	                    "columns" : [
	                        {'data': 'id'},
	                        {'data': 'email'},
	                        {'data': 'password'},
	                        {'data': 'firstname'},
	                        {'data': 'lastname'},
	                        {'data': 'phonenumber'},
	                        {'data': 'timestamp'},
	                    ],
	                    columnDefs: [ 
	                        { "width": "5%",  "targets": 0 , className:'text-center', render:function(data, type, row){
	                        	return data;
	                        }},
	                        { "width": "5%", "targets": 1 , className:'text-center', render:function(data, type, row){
	                        	return data;
	                        }},
	                        { "width": "5%",  "targets": 2 , className:'text-center', render:function(data, type, row){
	                        	// const linkurl = `${base_url}clients/detail/${row.id}`;
	                        	// const html =  `<a class = 'user-name-link' href = '${linkurl}'>${data}</a>`;
								// return html;
								return data;
	                        }},
	                        { "width": "5%",  "targets": 3 , className:'text-center', render:function(data, type, row){
								// const html =  `<a class = 'user-name-link' href = '#' onclick = onSendSMS(` + data + `)>` + data + `</a>`;
								// return html;
								return data;
	                        }},
	                        { "width": "5%", 	"targets": 4 , className:'text-center', render:function(data, type, row){
								return data;
	                        }},
	                        { "width": "5%", 	"targets": 5 , className:'text-center', render:function(data, type, row){
								return data;
	                        }},
	                        { "width": "5%", 	"targets": 6 , className:'text-center', render:function(data, type, row){
								return data;
	                        }},
	                        { "width": "5%", "targets": 7 , className:'delete', render:function(data, type, row){
	                        	const html = `<td class="delete"><i class="fa fa-trash fa-remove" data-id="${row.id}"></td>`;
	                        	return html;
	                        }},
	                    ],
				        "drawCallback": function( settings ) {
	                    	var info = datatable.page.info();
                            datatable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                cell.innerHTML = i+1 + info.start;
                            } );
                     	}
	    });

		// $(document).on('click',"i.fa-edit", function(){
		// 	const linkurl = $(this).attr('data-link');
		// 	location.href = linkurl;
		// });

		$(document).on('click',"i.fa-remove", function(){
			const id = $(this).data('id');
			var x = confirm("Are you sure you want to delete?");		
			var url = base_url + 'api/delete';
			if(x) {
				$.post(url, {
					id: id
				}, function(data, status) {
					// alert(data);
					location.href = ""
				});
			} else {
				return false;
			}
		});
    }
});

function onSendSMS(text){
	var person = prompt("Send to " + text, "Please input sms message content!");
		
	var url = base_url + 'clients/send_sms';
	$.post(url, {
		number: text,
		content: person
	}, function(data, status){
		// alert("sent");
		alert(data);
	});
}