$(document).ready(function() {
	$(".styled, .multiselect-container input").uniform({
		radioClass: 'choice'
	});

	$("i.fa-edit").on('click', function() {
		id = $(this).data('id');
		location.href = base_url + 'booking/edit/' + id;
	});

	$("i.fa-remove").on('click', function() {
		id = $(this).data('id');
		var x = confirm("Are you sure you want to delete?");
		var url = base_url + 'v1/booking/delete';
		if(x) {
			$.post(url, {
				id: id
			}, function(data, status) {
				location.href = ""
			});
		} else {
			return false;
		}
	});
});