/**
 * Select2 <Language> translation.
 */
(function ($) {
    "use strict";

    $.extend($.fn.select2.defaults, {
        formatNoMatches: function () { return "결과 없음"; },
        formatInputTooShort: function (input, min) { var n = min - input.length; return "입력문자길이가 너무 짧습니다. "+n+"글자를 더 입력해주십시오."; },
        formatInputTooLong: function (input, max) { var n = input.length - max; return "입력문자길이가 너무 깁니다. "+n+"글자를 지워주십시오."; },
        formatSelectionTooBig: function (limit) { return "최대 "+limit+"개까지만 선택할수 있습니다."; },
        formatLoadMore: function (pageNumber) { return "적재중..."; },
        formatSearching: function () { return "검색중..."; }
    });
})(jQuery);
