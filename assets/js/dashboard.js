/* ------------------------------------------------------------------------------
 *
 *  # C3.js - bars and pies
 *
 *  Demo setup of bars, pies and chart combinations
 *
 *  Version: 1.0
 *  Latest update: August 1, 2015
 *
 * ---------------------------------------------------------------------------- */

$(function () {
    
    // Initialize with options
    $('.daterange-datemenu').daterangepicker({
        showDropdowns: true,
        opens: "left",
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default'
    });


    // Pie chart
    // ------------------------------
    // Generate chart
    var pie_chart = c3.generate({
        bindto: '#c3-pie-chart',
        size: { width: 350 },
        color: {
            pattern: ['#3F51B5', '#FF9800', '#4CAF50', '#00BCD4', '#F44336']
        },
        data: {
            columns: [
                ['Chef', cntChef],
                ['Dine', cntDine],
            ],
            type : 'pie'
        },
        pie: {
			label:{
				format: function(value, ratio){
					return value; //returning here the value and not the ratio
				},
			}
		}
   	});   	

    var eleFrom = $('input[name="daterangepicker_start"]'),
    	eleTo = $('input[name="daterangepicker_end"]');

    $("button.applyBtn").on('click', function() {
    	var fromDate = eleFrom.val();
    	var toDate = eleTo.val();
    	var url = base_url + 'v1/restaurant/menu/range';
        $.post(url, {
            fromDate: fromDate,
            toDate: toDate
        }, function (data, message) {
            var re = /&quot;/g;
	        var aryMenuData = JSON.parse(data.replace(re, '"'));			
	        var aryResult = [];
	        var aryDate = [];

	        aryDate = getDates(fromDate, toDate);
	        aryResult.push('Menu');
            for (var i = 0; i < aryDate.length; i++) {
                aryResult.push(0);                
            };
            for (var i = 0; i < aryDate.length; i++) {
            	for (var j = 0; j < aryMenuData.length; j++) {
            		if(aryDate[i] == aryMenuData[j]['menu_day']) {
            			aryResult[i+1] = Number(aryMenuData[j]['cnt']);
            		}
            	}
            }
            var line_chart = c3.generate({
	            bindto: '#c3-line-chart',
	            point: { r: 4 },
	            size: { height: 450 },
	            color: { pattern: ['#4CAF50', '#F4511E','#1E88E5', '#FF00FF', '#8aae18', '#d008bb', '#a34500', '#4fd100', '#043ee4'] },
	            data: {
	                columns: [aryResult],
	                type: 'spline',
	            },
	            grid: {
	                y: {
	                    show: true,
	                    /*tick: {
	                        format: d3.format("$")
	                    }*/
	                },
	                x: {
	                    show: true,
	                    /*tick: {
	                        format: {
	                            title: function(d) { return aryDate[d]; }
	                        }
	                    }*/
	                }
	            },
	            tooltip: {
	                format: {
	                    title: function (d) { return aryDate[d]; },
	                    value: function (value, ratio, id) {
	                        var format = id === 'data1' ? d3.format(',') : d3.format('s');
	                        return format(value);
	                    }
	                }
	            }
	        });

        });
    });

    function getDates(startDate, stopDate) {
	    var dateArray = [];
	    var currentDate = moment(startDate);
	    var stopDate = moment(stopDate);
	    while (currentDate <= stopDate) {
	        dateArray.push( moment(currentDate).format('YYYY-MM-DD') )
	        currentDate = moment(currentDate).add(1, 'days');
	    }
	    return dateArray;
	}
});