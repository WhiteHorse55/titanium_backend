$(document).ready(function() {
	var re = /&quot;/g;
	var act = true; // true: add, false: edit
	var style_id = 0;
	var style_name = '';

	$("i.fa-edit").on('click', function() {
		const linkurl = $(this).attr('data-link');
		location.href = linkurl;
	});

	$("i.fa-remove").on('click', function() {
		const id = $(this).data('id');
		var x = confirm("Are you sure you want to delete?");		
		var url = base_url + 'v1/sale/request/delete';
		if(x) {
			$.post(url, {
				id: id
			}, function(data, status) {
				location.href = ""
			});
		} else {
			return false;
		}
	});

	initDataTable();

	function initDataTable(){
		var datatable = $('#sale-request-data-table').DataTable({
						"autoWidth": true,
				        "width": '100%',
	                    "processing": true,
	                    "serverSide": true,
	                    "scrollX": true,
	                    "fixedColumns":   {
				            leftColumns: 2,
				        },
				        // "deferRender": true,
	                    "ajax": {
	                        url:  `${base_url}v1/sale/getAllRequest`,
	                        type: "POST",
	                        "dataSrc": function (json) {
	                        	console.log(json);
	                            var return_data = new Array();
	                            for(var i=0; i< json.data.length; i++){
	                                return_data.push({
	                                'id': json.data[i].id,
	                                'no': json.data[i].no,
	                                'username'  : json.data[i].username,
	                                'useremail'  : json.data[i].useremail,
	                                'source_request'  : json.data[i].source_request,
	                                'price'  : json.data[i].price,
	                                'vincode'  : json.data[i].vincode,
	                                'zipcode'  : json.data[i].zipcode,
	                                'mileage'  : json.data[i].mileage,
	                                'year'  : json.data[i].year,
	                                'make'  : json.data[i].make,
	                                'model'  : json.data[i].model,
	                                'trimlevel'  : json.data[i].trimlevel,
	                                'userphone'  : json.data[i].userphone,
	                                'note'  : json.data[i].note,
	                                'date_time'  : moment(json.data[i].date_time).format('MM/D/YYYY h, h:mm:ss a'),
	                                'action'  : json.data[i].id,
	                                })
	                            }
	                            return return_data;
	                        }
	                    },
	                    "order": [[14,'desc']],
	                    "columns" : [
	                        {'data': 'id'},
	                        {'data': 'username'},
	                        {'data': 'useremail'},
	                        {'data': 'userphone'},
	                        {'data': 'source_request'},
	                        {'data': 'price'},
	                        {'data': 'vincode'},
	                        {'data': 'zipcode'},
	                        {'data': 'mileage'},
	                        {'data': 'year'},
	                        {'data': 'make'},
	                        {'data': 'model'},
	                        {'data': 'trimlevel'},
	                        
	                        {'data': 'note'},
	                        {'data': 'date_time'}
	                    ],
	                    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
	                    language: {
	                        search: '<span>Filter:</span> _INPUT_',
	                        lengthMenu: '<span>Show:</span> _MENU_',
	                        paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' },
	                        loadingRecords: '&nbsp;',
            				processing: '<div class="spinner"><i class="icon-spinner2 spinner"></i></div>'
	                    },
	                    columnDefs: [ 
	                        { "width": "5%", "targets": 0 , className:'text-center', render:function(data, type, row){

	                        	return data;
	                        }},
	                        { "width": "10%", "targets": 1 , className:'text-center', render:function(data, type, row){
	                        	const linkurl = `${base_url}sale/request/${row.id}`;
	                        	const html =  `<a class = 'user-name-link' href = '${linkurl}'>${data}</a>`;
	                        	return html;
	                        }},
	                        { "width": "10%", "targets": 2 , className:'text-center', render:function(data, type, row){
	                        	return data;
	                        }},
	                        { "width": "8%", "targets": 3 , className:'text-center', render:function(data, type, row){
	                        	return data;
	                        }},
	                        { "width": "5%", "targets": 4 , className:'text-center', render:function(data, type, row){
	                        	return data;
	                        }},
	                        { "width": "10%", "targets": 5 , className:'text-right', render:function(data, type, row){
	                        	return data;
	                        }},
	                        { "width": "10%", "targets": 6 , className:'text-left', render:function(data, type, row){
	                        	return data;
	                        }},
	                        { "width": "8%", "targets": 7 , className:'text-left', render:function(data, type, row){
	                        	return data;
	                        }},
	                        { "width": "5%", "targets": 8 , className:'text-right', render:function(data, type, row){
	                        	return data;
	                        }},
	                        { "width": "5%", "targets": 9 , className:'text-center', render:function(data, type, row){
	                        	return data;
	                        }},
	                        { "width": "5%", "targets": 10 , className:'text-center', render:function(data, type, row){
	                        	return data;
	                        }},
	                        { "width": "5%", "targets": 11 , className:'text-center', render:function(data, type, row){
	                        	return data;
	                        }},
	                        { "width": "6%", "targets": 12 , className:'text-center', render:function(data, type, row){
	                        	return data;
	                        }},
	                        { "width": "8%", "targets": 13 , className:'text-center', render:function(data, type, row){
	                        	return data;
	                        }},
	                        { "width": "8%", "targets": 14, className:'text-left', render:function(data, type, row){
	                        	return data;
	                        }},
	                        { "width": "5%", "targets": 15 , className:'edit', render:function(data, type, row){

	                        	const linkurl = `${base_url}sale/request/${row.id}`;
	                        	const html = `<i class="fa fa-edit" data-name="${row.useremail}" data-id="${row.id}" data-link = "${linkurl}"></i>`;
	                        	return html;
	                        }},
	                        { "width": "5%", "targets": 16 , className:'delete', render:function(data, type, row){
	                        	const html = `<td class="delete"><i class="fa fa-trash fa-remove" data-id="${row.id}"></td>`;
	                        	return html;
	                        }},
	                        
	                    ],

	                    "drawCallback": function( settings ) {
	                    	var info = datatable.page.info();
                            datatable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                cell.innerHTML = i+1 + info.start;
                            } );
                        }
	       	} );


			// Enable Select2 select for the length option
		    $('.dataTables_length select').select2({
		        minimumResultsForSearch: Infinity,
		        width: 'auto'
		    });
    }


});
