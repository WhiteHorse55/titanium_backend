$(document).ready(function() {

	initDataTable();

	function initDataTable(){
		var datatable = $('#table').DataTable({
						"autoWidth": true,
				        "width": '100%',
	                    "processing": true,
	                    "serverSide": true,
	                    "scrollX": true,
	                    "fixedColumns":   {
				            leftColumns: 1,
				        },
				        // "deferRender": true,
	                    "ajax": {
	                        url:  `${base_url}api_advertising/getSearchData`,
	                        type: "POST",
	                        "dataSrc": function (json) {
	                        	console.log(json);
	                            var return_data = new Array();
	                            for(var i=0; i< json.data.length; i++){
	                                return_data.push({
									'id': json.data[i].id,
	                                'company'   : json.data[i].company,
	                                'title'     : json.data[i].title,
	                                'type'      : json.data[i].type,
	                                'video'     : json.data[i].video,
	                                'image1'    : json.data[i].image1,
	                                'image2'    : json.data[i].image2,
	                                'image3'    : json.data[i].image3,
	                                'view_count'  : json.data[i].view_count,
	                                'timestamp' : json.data[i].timestamp,
	                                });
	                            }
	                            return return_data;
	                        }
	                    },

	                    "order": [[0,'desc']],
	                    "columns" : [
	                        {'data': 'id'},
	                        {'data': 'company'},
	                        {'data': 'title'},
	                        {'data': 'type'},
	                        {'data': 'video'},
	                        {'data': 'image1'},
	                        {'data': 'image2'},
	                        {'data': 'image3'},
	                        {'data': 'view_count'},
	                        {'data': 'timestamp'},
	                    ],
	                    columnDefs: [ 
	                        { "width": "5%",    "targets": 0 , className:'text-center', render:function(data, type, row){
	                        	return data;
	                        }},
	                        { "width": "5%",    "targets": 1 , className:'text-center', render:function(data, type, row){
	                        	return data;
	                        }},
	                        { "width": "5%",    "targets": 2 , className:'text-center', render:function(data, type, row){
								return data;
	                        }},
	                        { "width": "5%",    "targets": 3 , className:'text-center', render:function(data, type, row){
								return data;
	                        }},
	                        { "width": "5%",    "targets": 4 , className:'text-center', render:function(data, type, row){
                                if(data == ""){
                                    return "";
                                }
                                const html = `<td class=""><video width="100" height="100" controls src="${base_url+data}">
                                None</video></td>`;
	                        	return html;
	                        }},
	                        { "width": "5%", 	"targets": 5 , className:'text-center', render:function(data, type, row){
                                if(data == ""){
                                    return "";
                                }
                                const html = `<td class=""><img width="100" height="100" controls src="${base_url+data}">
                                </td>`;
	                        	return html;
	                        }},
	                        { "width": "5%", 	"targets": 6 , className:'text-center', render:function(data, type, row){
                                if(data == ""){
                                    return "";
                                }
                                const html = `<td class=""><img width="100" height="100" controls src="${base_url+data}">
                                </td>`;
	                        	return html;
	                        }},
	                        { "width": "5%", 	"targets": 7 , className:'text-center', render:function(data, type, row){
                                if(data == ""){
                                    return "";
                                }
                                const html = `<td class=""><img width="100" height="100" controls src="${base_url+data}">
                                </td>`;
	                        	return html;
	                        }},
	                        { "width": "5%", 	"targets": 8 , className:'text-center', render:function(data, type, row){
								return data;
	                        }},
	                        { "width": "5%", 	"targets": 9 , className:'text-center', render:function(data, type, row){
								return data;
	                        }},
	                        { "width": "5%", "targets": 10 , className:'delete', render:function(data, type, row){
	                        	const html = `<td class="delete"><i class="fa fa-trash fa-remove" data-id="${row.id}"></td>`;
	                        	return html;
	                        }},
	                    ],
				        "drawCallback": function( settings ) {
	                    	var info = datatable.page.info();
                            datatable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                cell.innerHTML = i+1 + info.start;
                            } );
                     	}
	    });

		// $(document).on('click',"i.fa-edit", function(){
		// 	const linkurl = $(this).attr('data-link');
		// 	location.href = linkurl;
		// });

		$(document).on('click',"i.fa-remove", function(){
			const id = $(this).data('id');
			var x = confirm("Are you sure you want to delete?");		
			var url = base_url + 'api_advertising/delete';
			if(x) {
				$.post(url, {
					id: id
				}, function(data, status) {
					// alert(data);
					location.href = ""
				});
			} else {
				return false;
			}
		});
    }
});

function onSendSMS(text){
	var person = prompt("Send to " + text, "Please input sms message content!");
		
	var url = base_url + 'clients/send_sms';
	$.post(url, {
		number: text,
		content: person
	}, function(data, status){
		// alert("sent");
		alert(data);
	});
}