$(document).ready(function() {
	// Basic
	$('.select').select2();
	$(".file-styled").uniform({
        fileButtonClass: 'action btn bg-pink-400'
    });
    
    $(".touchspin-price").TouchSpin({
        min: 0,
        max: 1000,
        step: 1,
        decimals: 0,
        prefix: '$'
    });

    $(".touchspin-distance").TouchSpin({
        min: 0,
        max: 1000,
        step: 1,
        decimals: 0,
        prefix: 'km'
    });


	var re = /&quot;/g;
	var act = true; //true: add, false: edit
	var aryRestaurant = filterDataById(JSON.parse(tmpRestaurantData.replace(re, '"')));
	var input = $("input.form-input");
	var textarea = $("textarea.form-input");
	var select = $("select.form-input");
	var id = 0;

	var $form = $('form.form_restaurant'),
		$panel = $('div.restaurant-panel'),
		hidden = $form.find( 'input[name="id"]'),
		name = $form.find( 'input[name="name"]'),
		user = $form.find( 'select[name="user_id"]'),
		message = $form.find( 'textarea[name="welcome_message"]'),
		latitude = $form.find( 'input[name="lat"]'),
		longitude = $form.find( 'input[name="lng"]'),
		address = $form.find( 'input[name="address"]'),
		removephotoname = $("input[name='removephotoname']"),
		gallery = $form.find('div.gallery');
	gallery.hide();
	$(document).on('click', '#btn_action', function(e) {		
		e.preventDefault();
		var check = true;

		for (var i = 0; i < input.length; i++) {
			if(validate(input[i]) == false) {
				showValidate(input[i]);
				check = false;
			}
		}

		for (var i = 0; i < textarea.length; i++) {
			if(validate(textarea[i]) == false) {
				showValidate(textarea[i]);
				check = false;
			}
		}

		for (var i = 0; i < select.length; i++) {
			if(validate(select[i]) == false) {
				showValidate(select[i]);
				check = false;
			}	
		}

		if(check) {
			$form.submit();
		} else {
			e.preventDefault();
		}
	});	

	$("input.form-control").each(function() {
		$(this).focus(function() {			
			hideValidate(this);
		});
	});

	$("textarea.form-control").each(function() {
		$(this).focus(function() {
			hideValidate(this);
		});
	});

	$("select.select").each(function() {		
		$(this).change(function() {			
			hideValidate(this);
		});
	});

	function validate(input) {
		if($(input).val().trim() == ''){
			return false;
		}
	}

	function showValidate(input) {        
		$(input).addClass('required');
		$(input).parent().find("span.select2").addClass("required");
	}

	function hideValidate(input) {
		$(input).removeClass('required');		
		$(input).parent().find("span.select2").removeClass("required");
	}

	$(document).on('click', 'i.fa-edit', function() {
		act = false;
		id = $(this).data('id');
		$("button#btn_cancle").fadeIn('fast');
		$form.attr('action', base_url + 'v1/restaurant/update');

		hidden.val(id);
		name.val(aryRestaurant[id].name);		
		user.find('option[value="' + aryRestaurant[id].user_id + '"]').attr('selected', 'true');
		user.parent().find("span.select2-selection__placeholder").remove();
		user.parent().find("span.select2-selection__rendered").empty();
		user.parent().find("span.select2-selection__rendered").append(aryRestaurant[id].first_name + ' ' + aryRestaurant[id].last_name);
		message.val(aryRestaurant[id].welcome_message);
		latitude.val(aryRestaurant[id].lat);
		longitude.val(aryRestaurant[id].lng);
		address.val(aryRestaurant[id].address);

		if(aryRestaurant[id].photo != '') {
			gallery.find('img.origin').attr('src', base_url + 'upload/restaurant/' + id + '/' + aryRestaurant[id].photo);
			gallery.show();
		}

		$form.removeClass('expand').addClass('collapse');
		$panel.removeClass('expand').addClass('collapse');
		$panel.animate()
	});

	$(document).on('click', 'i.fa-remove', function() {
		id = $(this).data('id');
		var x = confirm("Are you sure you want to delete?");		
		var url = base_url + 'v1/restaurant/delete';
		if(x) {
			$.post(url, {
				id: id
			}, function(data, status) {
				location.href = ""
			});
		} else {
			return false;
		}
	});

	$("button#btn_cancel").on('click', function() {
		act = true;
		$form.attr('action', base_url + 'v1/restaurant/update');
		hidden.val('');
		$(this).fadeOut('fast');
	});

	$("img.remove").on('click', function() {
		var aryRemoveName = [];
		aryRemoveName.push(aryRestaurant[id].photo);
		removephotoname.val(aryRemoveName.toString());
		$(this).parent().remove();
	});

	function filterDataById(ary) {
		var result = [];
		for (var i = 0; i < ary.length; i++) {
			result[ary[i]['id']] = ary[i];
		}
		return result;
	}

	$("button#btn_filter").on('click', function() {

		price = $('input[name="price"]');
		distance = $('input[name="distance"]');
		date = $('input[name="date"]');
		user_lat = 40.004119;
		user_lng = 116.474265;
		url = base_url + 'v1/restaurant/filter';

		var check = true;
		if(price.val() == 0 || distance.val() == 0 || date.val() == '') {
			check = false;
			alert("Please input all filter field");
		}
		
		if(check) {
			$.post(url, {
				price: price.val(),
				distance: distance.val(),
				date: date.val(),
				user_lat: user_lat,
				user_lng: user_lng
			}, function(data, status) {
				var re = /&quot;/g;
				var result = JSON.parse(data.replace(re, '"'));
				var table = $("table.table").dataTable();
				oSettings = table.fnSettings();
				table.fnClearTable(this);			
				for (var i = 0; i < result.length; i++)
				{
					var aiNew = table.fnAddData(['', '', '', '', '', '', '', '', '']);
					var nRow = table.fnGetNodes(aiNew[0]);
					editRow(table, nRow, result[i], Number(i + 1));
				}
			});
		}		
	});

	$("button#btn_cancel_filter").on('click', function() {
		price = $('input[name="price"]');
		distance = $('input[name="distance"]');
		date = $('input[name="date"]');
		if(price.val() != 0 && distance.val() != 0) {
			price.val(0);
			distance.val(0);
			date.val('');
			var table = $("table.table").dataTable();
			oSettings = table.fnSettings();
			table.fnClearTable(this);
			result = JSON.parse(tmpRestaurantData.replace(re, '"'));
			for (var i = 0; i < result.length; i++)
			{
				var aiNew = table.fnAddData(['', '', '', '', '', '', '', '', '']);
				var nRow = table.fnGetNodes(aiNew[0]);
				editRow(table, nRow, result[i], Number(i + 1));
			}
		}
	});

	function editRow(oTable, nRow, aData, index) {		
        var jqTds = $('>td', nRow);
        jqTds[0].innerHTML = index;
        jqTds[1].innerHTML = aData['name'];
        jqTds[2].innerHTML = aData['first_name'] + ' ' + aData['last_name'];
        jqTds[3].innerHTML = aData['welcome_message'];
        jqTds[4].innerHTML = aData['lat'];
        jqTds[5].innerHTML = aData['lng'];
        jqTds[6].innerHTML = aData['address'];        
        jqTds[7].innerHTML = '<i class="fa fa-edit" data-id="' + aData['id'] + '"></i>';
        jqTds[8].innerHTML = '<i class="fa fa-trash fa-remove" data-id="' + aData['id'] + '"></i>';
	}
});