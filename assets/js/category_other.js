$(document).ready(function() {
	var re = /&quot;/g;
	var act = true; // true: add, false: edit
	var other_id = 0;
	var other_name = '';

	$("#btn_action").on('click', function(e) {
		e.preventDefault();
		var $form = $('form.form_other'),
			n = $form.find( 'input[name="name"]');
		if(n.val().trim() == '') {
			alert("Please input all fields.");
		} else {
			if(act) {
				var url = base_url + 'category/add_other';
				$.post(url, {
					name: n.val()
				}, function(data, status) {
					objData = JSON.parse(data.replace(re, '"'));
					var msgElement = $("p.status_message");
					msgElement.empty();
					msgElement.append(objData.msg);
					msgElement.fadeIn('fast');
					msgElement.fadeOut(1500);
					setTimeout(function() { location.href = "" }, 1200);
				});
			} else {
				var url = base_url + 'category/update_other';				

				$.post(url, {
					id: other_id,
					name: n.val(),
				}, function(data, status) {
					var msgElement = $("p.status_message");
					msgElement.empty();
					msgElement.append("Success to update other");
					msgElement.fadeIn('fast');
					msgElement.fadeOut(1500);
					setTimeout(function() { location.href = "" }, 1200);
				});
			}	
		}
	});

	$("i.fa-edit").on('click', function() {
		act = false;
		$("button#btn_cancle").fadeIn('fast');
		var inputElement = $("input[name='name']");
		other_id = $(this).data('id');
		other_name = $(this).data('name');
		inputElement.val(other_name);
	});

	$("i.fa-remove").on('click', function() {
		other_id = $(this).data('id');
		var x = confirm("Are you sure you want to delete?");		
		var url = base_url + 'category/delete_other';
		if(x) {
			$.post(url, {
				id: other_id
			}, function(data, status) {
				location.href = ""
			});
		} else {
			return false;
		}
	});

	$("button#btn_cancle").on('click', function() {
		act = true;
		$("input[name='name']").val('');
		$(this).fadeOut('fast');
	});
});