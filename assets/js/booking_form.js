$(document).ready(function() {

	$('.select').select2();
	/*$('#datetimepicker1').datetimepicker({
		format:'YYYY-MM-DD hh:mm:00 a'
	});*/
	$('.datepicker').datepicker({
		format: 'yy-mm-dd'
	});
	$(".styled, .multiselect-container input").uniform({
		radioClass: 'choice'
	});

	var re = /&quot;/g;
    var input = $("input.form-control.form-input");
	var textarea = $("textarea.form-control");
	var select = $("select.select");

	$(".btn_action").on('click', function(e) {
		e.preventDefault();
		var check = true;

		for (var i = 0; i < input.length; i++) {
			if(validate(input[i]) == false) {
				showValidate(input[i]);
				check = false;
			}
		}

		for (var i = 0; i < textarea.length; i++) {
			if(validate(textarea[i]) == false) {
				showValidate(textarea[i]);
				check = false;
			}
		}

		for (var i = 0; i < select.length; i++) {
			if(validate(select[i]) == false) {
				showValidate(select[i]);
				check = false;
			}	
		}
		
		if(check) {			
			// $('form.form_booking').submit();
			var url = $('form.form_booking').attr('action');
			var status = $('input[name="status"]').parent().hasClass('checked') ? "on" : "off";
			if(action == "add") {
				$.post(url, {
					user_id: $('select[name="user_id"]').val(),
					menu_id: $('select[name="menu_id"]').val(),
					takeaway_plate: $('input[name="takeaway_plate"]').val(),
					dine_plate: $('input[name="dine_plate"]').val(),
					booking_date: $('input[name="booking_date"]').val(),
					status: status,
				}, function(data, status) {
					var jsonData = JSON.parse(data.replace(re, '"'));
					if(jsonData['success'] == 1) {
						location.href = base_url + 'booking';
					} else {
						var statusElement = $("p.status_error_message");
						statusElement.empty();
						statusElement.html(jsonData['msg']);
						statusElement.fadeIn('fast');
						setTimeout(function() {
							statusElement.fadeOut('fast');
						}, 2500);
					}					
				});
			} else if(action == "update") {
				$.post(url, {
					id: $('input[name="id"]').val(),
					user_id: $('select[name="user_id"]').val(),
					menu_id: $('select[name="menu_id"]').val(),
					takeaway_plate: $('input[name="takeaway_plate"]').val(),
					dine_plate: $('input[name="dine_plate"]').val(),
					booking_date: $('input[name="booking_date"]').val(),
					status: status,
				}, function(data, status) {					
					var jsonData = JSON.parse(data.replace(re, '"'));
					if(jsonData['success'] == 1) {
						location.href = base_url + 'booking';
					} else {
						var statusElement = $("p.status_error_message");
						statusElement.empty();
						statusElement.html(jsonData['msg']);
						statusElement.fadeIn('fast');
						setTimeout(function() {
							statusElement.fadeOut('fast');
						}, 2500);
					}
				});
			}
		}
	});

	$("input.form-control").each(function() {
		$(this).focus(function() {
			hideValidate(this);
		});
	});

	$("textarea.form-control").each(function() {
		$(this).focus(function() {
			hideValidate(this);
		});
	});

	$("select.select").each(function() {
		$(this).change(function() {
			hideValidate(this);
		});
	});

	function validate(input) {
		if($(input).val().trim() == ''){
			return false;
		}
	}

	function showValidate(input) {
		$(input).addClass('required');
		$(input).parent().find("span.select2").addClass("required");
	}

	function hideValidate(input) {
		$(input).removeClass('required');
		$(input).parent().find("span.select2").removeClass("required");
	}


	var stripe = Stripe('pk_test_k7IuJEH2DEAXPRtitfhD05WC');

	// Create an instance of Elements.
	var elements = stripe.elements();

	// Custom styling can be passed to options when creating an Element.
	// (Note that this demo uses a wider set of styles than the guide below.)
	var style = {
	  base: {
	    color: '#32325d',
	    lineHeight: '18px',
	    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
	    fontSmoothing: 'antialiased',
	    fontSize: '16px',
	    '::placeholder': {
	      color: '#aab7c4'
	    }
	  },
	  invalid: {
	    color: '#fa755a',
	    iconColor: '#fa755a'
	  }
	};

	// Create an instance of the card Element.
	var card = elements.create('card', {style: style});

	// Add an instance of the card Element into the `card-element` <div>.
	card.mount('#card-element');

	// Handle real-time validation errors from the card Element.
	card.addEventListener('change', function(event) {
	  var displayError = document.getElementById('card-errors');
	  if (event.error) {
	    displayError.textContent = event.error.message;
	  } else {
	    displayError.textContent = '';
	  }
	});

	// Handle form submission.
	var form = document.getElementById('payment-form');
	form.addEventListener('submit', function(event) {
	  event.preventDefault();

	  stripe.createToken(card).then(function(result) {
		console.log("Result = ");
		console.log(result);
	  });
	});
});