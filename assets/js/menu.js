$(document).ready(function() {
	$('.select').select2();
	$('.datepicker').datepicker({
		format: 'yy-mm-dd'
	});	
	$(document).on('click', 'i.fa-edit', function() {
		id = $(this).data('id');
		location.href = base_url + 'restaurant/menu/edit/' + id;
	});

	$(document).on('click', 'i.fa-remove', function() {
		id = $(this).data('id');
		var x = confirm("Are you sure you want to delete?");
		var url = base_url + 'v1/restaurant/menu/delete';
		if(x) {
			$.post(url, {
				id: id
			}, function(data, status) {
				location.href = ""
			});
		} else {
			return false;
		}
	});

	$("button#btn_filter").on('click', function() {

		restaurant = $('select[name="restaurant_id"]');
		style = $('select[name="style_id"]');
		base = $('select[name="base_id"]');
		other = $('select[name="other_id"]');
		menu_day = $('input[name="menu_day"]');		

		url = base_url + 'v1/restaurant/menu/filter';
		$.post(url, {
			restaurant: restaurant.val(),
			style: style.val(),
			base: base.val(),
			other: other.val(),
			menu_day: menu_day.val(),
		}, function(data, status) {
			var re = /&quot;/g;
			var result = JSON.parse(data.replace(re, '"'));
			var table = $("table.table").dataTable();
			oSettings = table.fnSettings();
			table.fnClearTable(this);			
			for (var i = 0; i < result.length; i++)
			{
				var aiNew = table.fnAddData(['', '', '', '', '', '', '','', '', '', '', '', '']);
				var nRow = table.fnGetNodes(aiNew[0]);				
				editRow(table, nRow, result[i]);
			}
		});
	});

	function editRow(oTable, nRow, aData) {		
        var jqTds = $('>td', nRow);
        jqTds[0].innerHTML = aData['title'];
        jqTds[1].innerHTML = aData['restaurant_name'];
        jqTds[2].innerHTML = aData['style_name'];
        jqTds[3].innerHTML = aData['base_name'];
        jqTds[4].innerHTML = aData['other_name'];
        jqTds[5].innerHTML = aData['price'];
        jqTds[6].innerHTML = aData['max_takeaway'];
        jqTds[7].innerHTML = aData['max_dine'];
        jqTds[8].innerHTML = aData['open_time'];
        jqTds[9].innerHTML = aData['close_time'];
        jqTds[10].innerHTML = aData['menu_day'];
        jqTds[11].innerHTML = '<i class="fa fa-edit" data-id="' + aData['id'] + '"></i>';
        jqTds[12].innerHTML = '<i class="fa fa-trash fa-remove" data-id="' + aData['id'] + '"></i>';
	}
});