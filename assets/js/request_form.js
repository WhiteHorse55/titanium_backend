$(document).ready(function() {

	var selectedYear = $(".select[name='year']").val();
	var selectedMake = $(".select[name='make']").val();
	var selectedModel = $(".select[name='model']").val();

	getYearsMakesModelsTrimlevels(selectedYear, selectedMake, selectedModel, (years, makes, models, trimlevels)=>{
		initYearSelect(years);
		initMakeSelect(makes);
		initModelSelect(models);
		initTrimSelect(trimlevels);
	});

	initPhoneNumberInput();

	function initPhoneNumberInput(){
		 $("#user-phone-number").intlTelInput({});
	}

	function initYearSelect(years){

		var items = years.map((item, index)=>{
			return `<option value = '${item.item}'>${item.item}</option>`;
		})

		$('.select[name="year"] optgroup').html(items);
		$('.select[name="year"]').select2();
		$('.select[name="year"]').on('select2:selecting', function(e, item, index){
			console.log($(this).val())
		});

	}

	function initMakeSelect(makes){

		var items = makes.map((item, index)=>{
			return `<option value = '${item.item}'>${item.item}</option>`;
		})

		$('.select[name="make"] optgroup').html(items).select2()

	}

	function initModelSelect(models){

		var items = models.map((item, index)=>{
			return `<option value = '${item.item}'>${item.item}</option>`;
		})

		$('.select[name="model"] optgroup').html(items).select2();

	}

	function initTrimSelect(trims){

		var items = trims.map((item, index)=>{
			return `<option value = '${item.item}'>${item.item}</option>`;
		})
		$('.select[name="trimlevel"] optgroup').html(items).select2();

	}



	// Basic
	$('.select').select2();
	
	var input = $("input.form-control.form-input");
	var textarea = $("textarea.form-control");
	var select = $("select.select");
	var aryRemove = [];
	var aryRemoveName = [];

	$("img.remove").on('click', function() {
    	var id = $(this).data('id');
    	aryRemove.push(id);
    	removephoto.val(aryRemove.toString());

    	var name = $(this).data('name');
    	aryRemoveName.push(name);
    	removephotoname.val(aryRemoveName.toString());

    	$(this).parent().remove();
    });

	$(".btn_add").on('click', function(e) {
		e.preventDefault();
		var check = true;

		for (var i = 0; i < input.length; i++) {
			if(validate(input[i]) == false) {
				showValidate(input[i]);
				check = false;
			}
		}

		for (var i = 0; i < textarea.length; i++) {
			if(validate(textarea[i]) == false) {
				showValidate(textarea[i]);
				check = false;
			}
		}

		for (var i = 0; i < select.length; i++) {
			if(validate(select[i]) == false) {
				showValidate(select[i]);
				check = false;
			}	
		}
		if(check) {			
			$('form.form_menu').submit();			
		}
	});

	$("input.form-control").each(function() {
		$(this).focus(function() {
			hideValidate(this);
		});
	});

	$("textarea.form-control").each(function() {
		$(this).focus(function() {
			hideValidate(this);
		});
	});

	$("select.select").each(function() {		
		$(this).change(function() {			
			hideValidate(this);
		});
	});

	function validate(input) {
		if($(input).val().trim() == ''){
			return false;
		}
	}

	function showValidate(input) {
		$(input).addClass('required');
		$(input).parent().find("span.select2").addClass("required");
	}

	function hideValidate(input) {
		$(input).removeClass('required');
		$(input).parent().find("span.select2").removeClass("required");
	}


	function getYearsMakesModelsTrimlevels(year, make, model, callback){

		var years = [];
		var makes = [];
		var models = [];
		var trimlevels = [];
		var selectedYear = year;
		var selectedMake = make;
		var selectedModel = model;

		var urlYears = "https://marketcheck-test.apigee.net/v1/search?api_key=RdJ3GMu7OzhFZGKvswBFzRMhABIlgjHd&rows=0&facets=year|0|1000&facet_sort=index";
		var settings = {
			"url": urlYears,
			"method": "GET",
		}

		$.ajax(settings).done(function(response){

			if(!response.facets){
				console.log("retry again");
				callback(years, makes, models,trimlevels);
				return
			}

			years = response.facets.year;

			if(selectedYear == "" || years.length == 0){
				callback(years, makes, models,trimlevels);
				return;
			}

			var urlmakes = `https://marketcheck-test.apigee.net/v1/search?api_key=RdJ3GMu7OzhFZGKvswBFzRMhABIlgjHd&rows=0&year=${selectedYear}&facets=make|0|1000&facet_sort=index`;
			var settings = {
				"url": urlmakes,
				"method": "GET"
			}

			$.ajax(settings).done(function(response){

				if(!response.facets){
					console.log("retry again");
					callback(years, makes, models,trimlevels);
					return
				}


				makes = response.facets.make;

				if(selectedMake == "" || makes.length == 0){
					callback(years, makes, models,trimlevels);
					return;
				}

				var urlmodels = `https://marketcheck-test.apigee.net/v1/search?api_key=RdJ3GMu7OzhFZGKvswBFzRMhABIlgjHd&rows=0&make=${selectedMake}&facets=model|0|1000&facet_sort=index`;
				var settings = {
					"url": urlmodels,
					"method": "GET"
				}

				$.ajax(settings).done(function(response){

					if(!response.facets){
						console.log("retry again");
						callback(years, makes, models,trimlevels);
						return
					}

					models = response.facets.model;

					if(selectedModel == "" || models.length == 0){
						callback(years, makes, models,trimlevels);
						return;
					}



					var urlTrims = "https://marketcheck-test.apigee.net/v1/search?api_key=RdJ3GMu7OzhFZGKvswBFzRMhABIlgjHd&rows=0&year=" + selectedYear; 
					var complMaker = "&make=" + selectedMake;
					var complModel = "&model=" + selectedModel;
					var complemento = "&facets=trim|0|1000&facet_sort=index";
					var urlGetTrims = urlTrims + complMaker + complModel + complemento;

					var settings = {
						"url": urlGetTrims,
						"method": "GET",
					}

					$.ajax(settings).done(function(response){

						if(!response.facets){
							console.log("retry again");
							callback(years, makes, models,trimlevels);
							return
						}

						trimlevels = response.facets.trim;
						callback(years, makes, models,trimlevels);

					});

				});

			});
		});
	}

	//button api
	$("#save-request-detail").click(function(){

		var data = $("form[name='form_request'").serialize();
		var url  = $("form[name='form_request'").attr('action');
		console.log(data);
		$.post(url, data).done(function(response){
			const res =  JSON.parse(response);
			if(res.type == "success"){
				showAlert("Sucess!");
			}
		});
	})

	$("#print-exel-file").click(function(){
		
		location.href = $(this).attr("action");
	});

	$("#send-email-btn").click(function(){
		const url = $(this).attr("action");
		const id = $(this).attr("data-id");
		$.post(url, {id:id}).done(function(response){
			showAlert("Sucess!");
		});

	});

	$("#send-text-sms-btn").click(function(){

		const url = `${base_url}v1/sale/request/sendSMS`;
		const id = $(this).attr("data-id");
		$.post(url, {id:id}).done(function(response){

			showAlert("Sucess!");
		});

	});
	

	function showAlert(text){
		$(".alert.bg-info > .alert-text").text(text);
		$(".alert.bg-info").removeClass("hide");
		setTimeout(function(){
			$(".alert.bg-info").addClass("hide");
		},2000);
	}

});