<?php 
/**
 * User Model
 * Created by: arangde
 * Date: 11/21/13
 *
 */
class Model_advertising extends Base_model {
	
	protected $table;

	public function __construct() {
		parent::__construct();	
		$this->table = "tbl_advertising";
	}

    public function create($data){
		$this->db->insert($this->table, $data);
    }

    public function saveData($data){
        $this->db->replace($this->table, $data);
    }
    
	public function getPage($filterOpt){

		$sql = "SELECT * FROM $this->table ORDER BY timestamp DESC LIMIT ".$filterOpt["length"]." OFFSET ".$filterOpt["start"];
		$query = $this->db->query($sql);
		return $query->result_array();
	}

    // for admin panel table
	public function getAll($filterOpt){

		$sql = "SELECT * FROM $this->table WHERE "; 
		$sql .= "company LIKE '%".$filterOpt["search"]."%' OR ";
		$sql .= "title LIKE '%".$filterOpt["search"]."%' OR ";
		$sql .= "type LIKE '%".$filterOpt["search"]."%' OR ";
		$sql .= "video LIKE '%".$filterOpt["search"]."%' ";
		$sql .= "ORDER BY ".$filterOpt["order_field"]." ".$filterOpt["order_dir"]." ";
        $sql .= "LIMIT ".$filterOpt["length"]." OFFSET ".$filterOpt["start"];
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getTotalCount($filterOpt) {
		$sql = "SELECT * FROM $this->table WHERE "; 
		$sql .= "company LIKE '%".$filterOpt["search"]."%' OR ";
		$sql .= "title LIKE '%".$filterOpt["search"]."%' OR ";
		$sql .= "type LIKE '%".$filterOpt["search"]."%' OR ";
		$sql .= "video LIKE '%".$filterOpt["search"]."%' ";
		$sql .= "ORDER BY ".$filterOpt["order_field"]." ".$filterOpt["order_dir"]." ";
        $sql .= "LIMIT ".$filterOpt["length"]." OFFSET ".$filterOpt["start"];
		$query = $this->db->query($sql);
		return  $query->num_rows();
    }
    
    public function delete_item($arg){
        // $this->db->where('email', $email);
        // $this->db->delete($this->table);
        $this->db->delete($this->table, array('id' => $arg));
    }
}
