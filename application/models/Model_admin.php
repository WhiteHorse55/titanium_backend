<?php 
/**
 * User Model
 * Created by: arangde
 * Date: 11/21/13
 *
 */
class Model_admin extends Base_model {
	
	protected $table;

	public function __construct() {
		parent::__construct();	
		$this->table = "admin";		
	}
	
  public function getByName($str) { // by name
			$this->db->select("*");
			$this->db->from($this->table);
			$this->db->where("name", $str);
			$query = $this->db->get();
			return $query->row_array();
	}
}
