<?php 
/**
 * User Model
 * Created by: arangde
 * Date: 11/21/13
 *
 */
class Sale_model extends Base_model {
	
	protected $sale_request;

	public function __construct() {
		parent::__construct();	
		$this->sale_request = "sale_request";
		$this->users = "users";
	}

	public function getSaleRequests() {
		$this->db->select('*');
		$this->db->from($this->sale_request);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getTotalCount($filterOpt) {
		$sql = 'SELECT * FROM sale_request WHERE '; 
		$sql .= 'vincode LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'zipcode LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'useremail LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'username LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'year LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'make LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'model LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'trimlevel LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'price LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'source_request LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'note LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'userphone LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'mileage LIKE "%'.$filterOpt['search'].'%" ';
		$sql .= "ORDER BY ".$filterOpt['order_field']." ".$filterOpt['order_dir']." ";
		$query = $this->db->query($sql);
		return  $query->num_rows();
	}

	public function addSaleRequest($data, $signup = false)
	{
		return $this->add($this->sale_request, $data);
	}


	public function updateSaleRequest($data, $id) {
		if(isset($data['id']))
			unset($data['id']);
		
		$this->db->where('id', $id);
		return $this->db->update($this->sale_request, $data);
	}

	public function getSaleReqeustById($id) {
		$this->db->select('*');
		$this->db->from($this->sale_request);
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function deleteSaleRequest($id){

		$this->db->where('id', $id);
		return $this->db->delete($this->sale_request);
	}

	public function getAllSaleRequests($filterOpt){

		$sql = 'SELECT * FROM sale_request WHERE '; 
		$sql .= 'vincode LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'zipcode LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'username LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'useremail LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'year LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'make LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'model LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'trimlevel LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'price LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'source_request LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'note LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'userphone LIKE "%'.$filterOpt['search'].'%" OR ';
		$sql .= 'mileage LIKE "%'.$filterOpt['search'].'%" ';
		$sql .= "ORDER BY ".$filterOpt['order_field']." ".$filterOpt['order_dir']." ";
		$sql .= "LIMIT ".$filterOpt['length']." OFFSET ".$filterOpt['start'];
		
		$query = $this->db->query($sql);
		$result = $query->result_array();

		return $query->result_array();
	}

}
