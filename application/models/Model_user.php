<?php 
/**
 * User Model
 * Created by: arangde
 * Date: 11/21/13
 *
 */
class Model_user extends Base_model {
	
	protected $table;

	public function __construct() {
		parent::__construct();	
		$this->table = "tbl_user";
	}


    // for mobile api
    public function getByEmail($str){
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("email", $str);
		$query = $this->db->get();
		return $query->row_array();
    }

    public function getByPhonenumber($str){
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("phonenumber", $str);
		$query = $this->db->get();
		return $query->row_array();
    }

    public function getByName($str) { // by name
			$this->db->select("*");
			$this->db->from($this->table);
			$this->db->where("name", $str);
			$query = $this->db->get();
			return $query->row_array();
    }
		
		public function delete_item($arg){
			// $this->db->where('email', $email);
			// $this->db->delete($this->table);
			$this->db->delete($this->table, array('id' => $arg));
		}

    public function create($data){
			$this->db->insert($this->table, $data);
			$insert_id = $this->db->insert_id();
    }

    public function saveData($data){
        $this->db->replace($this->table, $data);
    }
    
    // for admin panel table
	public function getAll($filterOpt){

		$sql = "SELECT * FROM $this->table WHERE "; 
		$sql .= "firstname LIKE '%".$filterOpt["search"]."%' OR ";
		$sql .= "lastname LIKE '%".$filterOpt["search"]."%' OR ";
		$sql .= "phonenumber LIKE '%".$filterOpt["search"]."%' OR ";
		$sql .= "email LIKE '%".$filterOpt["search"]."%' ";
		$sql .= "ORDER BY ".$filterOpt["order_field"]." ".$filterOpt["order_dir"]." ";
        $sql .= "LIMIT ".$filterOpt["length"]." OFFSET ".$filterOpt["start"];
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getTotalCount($filterOpt) {
		$sql = "SELECT * FROM $this->table WHERE "; 
		$sql .= "firstname LIKE '%".$filterOpt["search"]."%' OR ";
		$sql .= "lastname LIKE '%".$filterOpt["search"]."%' OR ";
		$sql .= "phonenumber LIKE '%".$filterOpt["search"]."%' OR ";
		$sql .= "email LIKE '%".$filterOpt["search"]."%' ";
		$sql .= "ORDER BY ".$filterOpt["order_field"]." ".$filterOpt["order_dir"]." ";
        $sql .= "LIMIT ".$filterOpt["length"]." OFFSET ".$filterOpt["start"];
		$query = $this->db->query($sql);
		return  $query->num_rows();
	}
}
