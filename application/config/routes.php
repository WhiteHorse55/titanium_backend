<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
*/
$route['default_controller'] = 'auth';


// Admin User
$route['users'] = 'users';
$route['users/all'] = 'users/all';


$route['signup'] = 'auth/create';
$route['user/user_save'] = 'auth/store';
$route['login'] = 'auth/go_login';
$route['logout'] = 'auth/logout';

$route['dashboard'] = "dashboard";

$route['sale/request'] = "sale/go_request";
$route['v1/sale/getAllRequest'] = "sale/getAllRequest";
$route['sale/request/(:num)'] = "sale/request_detail/$1";
$route['v1/sale/request/update'] = "sale/update_request";
$route['v1/sale/request/delete'] = "sale/delete_request";

$route['v1/sale/request/printSaleRequest/(:num)'] = "sale/printSaleRequest/$1";
$route['v1/sale/request/sendEmail'] = "sale/sendEmail";
$route['v1/sale/request/sendSMS'] = "sale/sendSMS";


$route['category/style'] = "category/go_style";
$route['category/add_style'] = "category/add_style";
$route['category/update_style'] = "category/update_style";
$route['category/delete_style'] = "category/delete_style";

$route['category/base'] = "category/go_base";
$route['category/add_base'] = "category/add_base";
$route['category/update_base'] = "category/update_base";
$route['category/delete_base'] = "category/delete_base";

$route['category/other'] = "category/go_other";
$route['category/add_other'] = "category/add_other";
$route['category/update_other'] = "category/update_other";
$route['category/delete_other'] = "category/delete_other";

$route['restaurant'] = "restaurant";
$route['v1/restaurant/add'] = "restaurant/store";
$route['v1/restaurant/update'] = "restaurant/update";
$route['v1/restaurant/delete'] = "restaurant/destroy";
$route['v1/restaurant/filter'] = "restaurant/filter";

$route['restaurant/menu'] = "menu";
$route['restaurant/menu/new'] = "menu/create";
$route['restaurant/menu/edit/(:num)'] = "menu/show/$1";
$route['v1/restaurant/menu/filter'] = "menu/filter";
$route['v1/restaurant/menu/add'] = "menu/store";
$route['v1/restaurant/menu/update'] = "menu/update";
$route['v1/restaurant/menu/delete'] = "menu/destroy";
$route['v1/restaurant/menu/range'] = "menu/range";

$route['booking'] = "booking";
$route['booking/new'] = "booking/create";
$route['booking/edit/(:num)'] = "booking/show/$1";
$route['v1/booking/add'] = "booking/store";
$route['v1/booking/update'] = "booking/update";
$route['v1/booking/delete'] = "booking/destroy";

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
