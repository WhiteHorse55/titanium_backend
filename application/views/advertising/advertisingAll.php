<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href=""><i class="icon-home2 position-left"></i>Advertising</a></li>
			<li class="active">all</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h5 class="panel-title">Advertisings information</h5>
					<div class="heading-elements">
						<ul class="icons-list">
		            		<li><a data-action="collapse"></a></li>
		            		<li><a data-action="reload"></a></li>
		            		<li><a data-action="close"></a></li>
		            	</ul>
		        	</div>
				</div>
				<div class="panel-body">
				</div>
				<table class="display" id = "table">
					<thead>
						<tr>
							<th >No.</th>
							<th >Company</th>
							<th >Title</th>
							<th >Type</th>
							<th >Video</th>
							<th >Image1</th>
							<th >Image2</th>
							<th >Image3</th>
							<th >View</th>
							<th >Timestamp</th>
							<th >Delete</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/table_advertisingAll.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">