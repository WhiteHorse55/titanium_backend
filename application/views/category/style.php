<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Category</span> - Style</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href=""><i class="icon-home2 position-left"></i> Category</a></li>
			<li class="active">Style</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-4">
			<form name="form_style" class="form_style">
	            <div class="panel panel-flat">
					<div class="panel-heading">
		                <h6 class="panel-title">Add / Update Styles</h6>
					</div>

	                <div class="panel-body">
						<div class="form-group">
							<label>Style name:</label>
							<input type="text" name="name" class="form-control" placeholder="Eugene Kopyov">
						</div>
						<p class="status_message"></p>
						<div class="text-right">
							<button class="btn bg-teal-400" id="btn_action">Submit form <i class="icon-arrow-right14 position-right"></i></button>
							<button class="btn bg-indigo-300" id="btn_cancle">Cancle</button>
						</div>
					</div>
                </div>
            </form>
		</div>
		<div class="col-lg-8 col-md-8 col-sm-8">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h5 class="panel-title">Style tables</h5>
					<div class="heading-elements">
						<ul class="icons-list">
		            		<li><a data-action="collapse"></a></li>
		            		<li><a data-action="reload"></a></li>
		            		<li><a data-action="close"></a></li>
		            	</ul>
		        	</div>
				</div>
				<div class="panel-body">
					
				</div>
				<table class="table datatable-basic table-bordered dataTable no-footer">
					<thead>
						<tr>
							<th width="5%">No.</th>
							<th width="85%">Name</th>
							<th width="5%">Edit</th>
							<th width="5%">Delete</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($styles as $key => $value): ?>
							<tr>
								<td><?php echo $key + 1; ?></td>
								<td><?php echo $value['name']; ?></td>
								<td class="edit"><i class="fa fa-edit" data-name="<?php echo $value['name']; ?>" data-id="<?php echo $value['id']; ?>"></i></td>
								<td class="delete"><i class="fa fa-trash fa-remove" data-id="<?php echo $value['id']; ?>"></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/category_style.js"></script>