<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Restaurant</span> - Menu</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href=""><i class="icon-home2 position-left"></i> Restaurant</a></li>
			<li class="active">Menu</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h5 class="panel-title">Menu tables</h5>
					<div class="heading-elements">
						<ul class="icons-list">
		            		<li><a data-action="collapse"></a></li>
		            		<li><a data-action="reload"></a></li>
		            		<li><a data-action="close"></a></li>
		            	</ul>
		        	</div>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
							<div class="form-group">
								<select data-placeholder="Select restaurant" class="select" name="restaurant_id">
									<optgroup label="Registered Restaurants">
										<option value="">Cancel Filter</option>
										<?php foreach ($restaurants as $key => $value): ?>
											<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
										<?php endforeach; ?>
									</optgroup>
								</select>
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
							<div class="form-group">
								<select data-placeholder="Select style" class="select" name="style_id">
									<optgroup label="Registered Styles">
										<option value="">Cancel Filter</option>
										<?php foreach ($styles as $key => $value): ?>
											<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
										<?php endforeach; ?>
									</optgroup>
								</select>
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
							<div class="form-group">
								<select data-placeholder="Select base" class="select" name="base_id">
									<optgroup label="Registered Bases">
										<option value="">Cancel Filter</option>
										<?php foreach ($bases as $key => $value): ?>
											<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
										<?php endforeach; ?>
									</optgroup>
								</select>
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
							<div class="form-group">
								<select data-placeholder="Select other" class="select" name="other_id">
									<optgroup label="Registered Others">
										<option value="">Cancel Filter</option>
										<?php foreach ($others as $key => $value): ?>
											<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
										<?php endforeach; ?>
									</optgroup>
								</select>
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
							<div class="form-group">
								<div class="input-group date" data-provide="datepicker">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-th"></span>
									</div>
									<input type="text" class="form-control" name="menu_day">
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
							<div class="form-group">
								<button class="btn bg-teal-400" id="btn_filter">Filter <i class="icon-arrow-right14 position-right"></i></button>
							</div>
						</div>
					</div>
				</div>			
				<table class="table datatable-basic table-bordered dataTable no-footer">
					<thead>
						<tr>
							<th>Title</th>
							<th>Restaurant</th>
							<th>Style</th>
							<th>Base</th>
							<th>Other</th>
							<th>Price</th>
							<th>Takeaway</th>
							<th>Dine</th>
							<th>OpenTime</th>
							<th>CloseTime</th>
							<th>MenuDay</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>
					</thead>
					<tbody>
						<?php
							function cutLongString($in, $cntCut)
							{
								$out = strlen($in) > $cntCut ? substr($in, 0, $cntCut)."..." : $in;
								return $out;
							}
						?>
						<?php foreach ($menu as $key => $value): ?>
							<tr>
								<td><?php echo $value['title']; ?></td>
								<td><?php echo $value['restaurant_name']; ?></td>
								<td><?php echo $value['style_name']; ?></td>
								<td><?php echo $value['base_name']; ?></td>
								<td><?php echo $value['other_name']; ?></td>
								<td><?php echo $value['price']; ?></td>
								<td><?php echo $value['max_takeaway']; ?></td>
								<td><?php echo $value['max_dine']; ?></td>
								<td><?php echo $value['open_time']; ?></td>
								<td><?php echo $value['close_time']; ?></td>
								<td><?php echo $value['menu_day']; ?></td>
								<td class="edit"><i class="fa fa-edit" data-id="<?php echo $value['id']; ?>"></i></td>
								<td class="delete"><i class="fa fa-trash fa-remove" data-id="<?php echo $value['id']; ?>"></i></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/menu.js"></script>