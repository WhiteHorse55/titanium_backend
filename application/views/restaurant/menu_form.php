<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Restaurant</span> - Menu</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href=""><i class="icon-home2 position-left"></i> Restaurant</a></li>
			<li class="active">Menu</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">	
	<?php if(isset($menu)): ?>
		<form name="form_menu" class="form_menu form-horizontal" action="<?php echo base_url(); ?>v1/restaurant/menu/update" method="post" enctype="multipart/form-data">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title">Add / Update menus</h6>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6">
							<fieldset>
								<legend class="text-semibold"><i class="icon-reading position-left"></i> Menu basic infos</legend>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Enter menu title:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<input type="hidden" name="id" value="<?php echo $menu['id'];?>">
										<input type="text" class="form-control form-input" placeholder="Input menu title" name="title" value="<?php echo $menu['title']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Select Restaurant:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<select data-placeholder="Select restaurant" class="select" name="restaurant_id">
											<option></option>
											<optgroup label="Registered Restaurants">
												<?php foreach ($restaurants as $key => $value):
													$selected = "";
													if($value['id'] == $menu['restaurant_id']) $selected = " selected";
												?>
												<option value="<?php echo $value['id']; ?>"<?php echo $selected;?>><?php echo $value['name']; ?></option>
												<?php endforeach; ?>
											</optgroup>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Select Style:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<select data-placeholder="Select style" class="select" name="style_id">
											<option></option>
											<optgroup label="Registered Styles">
												<?php foreach ($styles as $key => $value): 
													$selected = "";
													if($value['id'] == $menu['style_id']) $selected = " selected";
												?>
												<option value="<?php echo $value['id']; ?>"<?php echo $selected;?>><?php echo $value['name']; ?></option>
												<?php endforeach; ?>
											</optgroup>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Select Base:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<select data-placeholder="Select base" class="select" name="base_id">
											<option></option>
											<optgroup label="Registered Bases">
												<?php foreach ($bases as $key => $value): 
													$selected = "";
													if($value['id'] == $menu['base_id']) $selected = " selected";
												?>
												<option value="<?php echo $value['id']; ?>"<?php echo $selected;?>><?php echo $value['name']; ?></option>
												<?php endforeach; ?>
											</optgroup>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Select Other:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<select data-placeholder="Select other" class="select" name="other_id">
											<option></option>
											<optgroup label="Registered Others">
												<?php foreach ($others as $key => $value): 
													$selected = "";
													if($value['id'] == $menu['other_id']) $selected = " selected";
												?>
												<option value="<?php echo $value['id']; ?>"<?php echo $selected;?>><?php echo $value['name']; ?></option>
												<?php endforeach; ?>
											</optgroup>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Select Country:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<select data-placeholder="Select country" class="select" name="country">
											<option></option>
											<optgroup label="Registered Country">
												<?php foreach ($countries as $key => $value): 
													$selected = "";
													if($value['short'] == $menu['country']) $selected = " selected";
												?>
												<option value="<?php echo $value['short']; ?>"<?php echo $selected;?>><?php echo $value['name']; ?></option>
												<?php endforeach; ?>
											</optgroup>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Description:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<textarea class="form-control" name="description" rows="6" value=""><?php echo $menu['description']; ?></textarea>
									</div>
								</div>
							</fieldset>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6">
							<fieldset>
								<legend class="text-semibold"><i class="icon-truck position-left"></i> Menu price and times</legend>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Set Price:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<input class="form-control form-input" type="text" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 46 || event.charCode == 0 " name="price" placeholder="Input Price" value="<?php echo $menu['price']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Set Maxium Takeaway:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<input class="form-control form-input" type="number" name="max_takeaway" placeholder="Input Maxium Takeaway" value="<?php echo $menu['max_takeaway']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Set Maxium Dine:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<input class="form-control form-input" type="number" name="max_dine" placeholder="Input Maxium Dine" value="<?php echo $menu['max_dine']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Set Open Time:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-alarm"></i></span>
											<input type="text" class="form-control pickatime-clear form-input" placeholder="Try me&hellip;" name="open_time" value="<?php echo $menu['open_time']; ?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Set Close Time:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-alarm"></i></span>
											<input type="text" class="form-control pickatime-clear form-input" placeholder="Try me&hellip;" name="close_time" value="<?php echo $menu['close_time']; ?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Set Serve Time:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-alarm"></i></span>
											<input type="text" class="form-control pickatime-clear form-input" placeholder="Try me&hellip;" name="serve_time" value="<?php echo $menu['serve_time']; ?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Set Menu Day:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<div class="input-group date" data-provide="datepicker">
											<div class="input-group-addon">
												<span class="glyphicon glyphicon-th"></span>
											</div>
											<input type="text" class="form-control" name="menu_day" value="<?php echo $menu['menu_day']; ?>">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Image Upload:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<input type="file" class="file-styled" name="upload_Files[]" multiple>		
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-lg-9 col-md-9 col-sm-9 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
											<div class="gallery">
												<input type="hidden" name="removephoto" value="">
												<input type="hidden" name="removephotoname" value="">
												<ul>
													<?php if(!empty($galleries)): foreach($galleries as $file): ?>
													<li>
														<img class="origin" src="<?php echo base_url();?>upload/menu/<?php echo $menu['id']; ?>/<?php echo $file['image']; ?>">
														<img class="remove" data-id="<?php echo $file['id']; ?>" data-name="<?php echo $file['image']; ?>" src="<?php echo base_url();?>assets/images/icons/remove.png">
													</li>
													<?php endforeach; else: ?>
													<?php endif; ?>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
							<div class="text-right">
								<button class="btn btn-primary btn_add">Submit form <i class="icon-arrow-right14 position-right"></i></button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	<?php else: ?>
		<form name="form_menu" class="form_menu form-horizontal" action="<?php echo base_url(); ?>v1/restaurant/menu/add" method="post" enctype="multipart/form-data">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title">Add / Update menus</h6>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6">
							<fieldset>
								<legend class="text-semibold"><i class="icon-reading position-left"></i> Menu basic infos</legend>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Enter menu title:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<input type="text" class="form-control form-input" placeholder="Input menu title" name="title">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Select Restaurant:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<select data-placeholder="Select restaurant" class="select" name="restaurant_id">
											<option></option>
											<optgroup label="Registered Restaurants">
												<?php foreach ($restaurants as $key => $value): ?>
													<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
												<?php endforeach; ?>
											</optgroup>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Select Style:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<select data-placeholder="Select style" class="select" name="style_id">
											<option></option>
											<optgroup label="Registered Styles">
												<?php foreach ($styles as $key => $value): ?>
													<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
												<?php endforeach; ?>
											</optgroup>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Select Base:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<select data-placeholder="Select base" class="select" name="base_id">
											<option></option>
											<optgroup label="Registered Bases">
												<?php foreach ($bases as $key => $value): ?>
													<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
												<?php endforeach; ?>
											</optgroup>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Select Other:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<select data-placeholder="Select other" class="select" name="other_id">
											<option></option>
											<optgroup label="Registered Others">
												<?php foreach ($others as $key => $value): ?>
													<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
												<?php endforeach; ?>
											</optgroup>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Select Country:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<select data-placeholder="Select country" class="select" name="country">
											<option></option>
											<optgroup label="Registered Country">
												<?php foreach ($countries as $key => $value): ?>
													<option value="<?php echo $value['short']; ?>"><?php echo $value['name']; ?></option>	
												<?php endforeach; ?>
											</optgroup>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Description:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<textarea class="form-control" name="description" rows="6"></textarea>
									</div>
								</div>
							</fieldset>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6">
							<fieldset>
								<legend class="text-semibold"><i class="icon-truck position-left"></i> Menu price and times</legend>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Set Price:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<input class="form-control form-input" type="text" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 46 || event.charCode == 0 " name="price" placeholder="Input Price">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Set Maxium Takeaway:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<input class="form-control form-input" type="number" name="max_takeaway" placeholder="Input Maxium Takeaway">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Set Maxium Dine:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<input class="form-control form-input" type="number" name="max_dine" placeholder="Input Maxium Dine">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Set Open Time:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-alarm"></i></span>
											<input type="text" class="form-control pickatime-clear form-input" placeholder="Try me&hellip;" name="open_time">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Set Close Time:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-alarm"></i></span>
											<input type="text" class="form-control pickatime-clear form-input" placeholder="Try me&hellip;" name="close_time">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Set Serve Time:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-alarm"></i></span>
											<input type="text" class="form-control pickatime-clear form-input" placeholder="Try me&hellip;" name="serve_time">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Set Menu Day:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<div class="input-group date" data-provide="datepicker">
											<div class="input-group-addon">
												<span class="glyphicon glyphicon-th"></span>
											</div>
											<input type="text" class="form-control form-input" name="menu_day">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 col-md-3 col-sm-3 control-label">Image Upload:</label>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<input type="file" class="file-styled" name="upload_Files[]" multiple>
										<span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
									</div>
								</div>
							</fieldset>
							<div class="text-right">
								<input type="submit" class="btn btn-primary btn_add">
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	<?php endif; ?>
</div>
<script type="text/javascript">
	var action = "<?php echo $action; ?>"
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/menu_form.js"></script>