<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Restaurant</span> - Restaurant</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href=""><i class="icon-home2 position-left"></i> Restaurant</a></li>
			<li class="active">Restaurant</li>
		</ul>
	</div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content cleardiv">
	<form name="form_Restaurant" class="form_restaurant form-horizontal pull-left expand" action="<?php echo base_url(); ?>v1/restaurant/add" method="post" enctype="multipart/form-data">
        <div class="panel panel-flat">
			<div class="panel-heading">
                <h6 class="panel-title">Add / Update Restaurants</h6>
			</div>

            <div class="panel-body">
				<div class="form-group">
					<label class="col-lg-2 col-md-2 col-sm-2 control-label" for="name">Name:</label>
					<div class="col-lg-10 col-md-10 col-sm-10">
						<input type="hidden" name="id">
						<input class="form-control form-input" type="text" name="name" placeholder="Input name...">
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-2 col-sm-2 control-label" for="user_id">User:</label>
					<div class="col-lg-10 col-md-10 col-sm-10">
						<select data-placeholder="Select user" class="select form-input" name="user_id">
							<option></option>
							<optgroup label="Registered Users">
								<?php foreach ($user as $key => $value): ?>
									<option value="<?php echo $value['id']; ?>"><?php echo $value['first_name'] . ' ' . $value['last_name']; ?></option>
								<?php endforeach; ?>
							</optgroup>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-2 col-sm-2 control-label" for="welcome_message">Message:</label>
					<div class="col-lg-10 col-md-10 col-sm-10">
						<textarea class="form-control form-input" name="welcome_message" placeholder="Welcome message..." rows="4"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-2 col-sm-2 control-label" for="lat">Latitude:</label>
					<div class="col-lg-10 col-md-10 col-sm-10">
						<input class="form-control form-input" type="text" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) ||  
								event.charCode == 46 || event.charCode == 0 " name="lat" placeholder="Input Latitude">
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-2 col-sm-2 control-label" for="lng">Longitude:</label>
					<div class="col-lg-10 col-md-10 col-sm-10">
						<input class="form-control form-input" type="text" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) ||  
								event.charCode == 46 || event.charCode == 0 " name="lng" placeholder="Input Longitude">
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-2 col-sm-2 control-label" for="address">Address:</label>
					<div class="col-lg-10 col-md-10 col-sm-10">
						<input class="form-control form-input" type="address" name="address" placeholder="Input Address">
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-2 col-sm-2 control-label">Upload:</label>
					<div class="col-lg-10 col-md-10 col-sm-10">
						<input type="file" class="file-styled" name="upload_file" >
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
							<div class="gallery">
								<input type="hidden" name="removephotoname" value="">
								<ul>
									<li>
										<img class="origin" src="">
										<img class="remove" data-id="" data-name="" src="<?php echo base_url();?>assets/images/icons/remove.png">
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<p class="status_message"></p>
				<div class="text-right">
					<button class="btn bg-teal-400" id="btn_action">Submit form <i class="icon-arrow-right14 position-right"></i></button>
					<button class="btn bg-indigo-300" id="btn_cancel">Cancel <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</div>
        </div>
    </form>
	<div class="panel panel-flat restaurant-panel pull-left expand">
		<div class="panel-heading">
			<h5 class="panel-title">Restaurant tables & Filter options</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            		<li><a data-action="close"></a></li>
            	</ul>
        	</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
					<div class="form-group">
						<label for="price">Price for filter:</label>
						<input type="text" name="price" value="0" class="touchspin-price">
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
					<div class="form-group">
						<label for="distance">Distance for filter:</label>
						<input type="text" name="distance" value="0" class="touchspin-distance">
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
					<div class="form-group">
						<label for="date">Date for filter:</label>
						<div class="input-group date" data-provide="datepicker">
							<div class="input-group-addon">
								<span class="glyphicon glyphicon-th"></span>
							</div>
							<input type="text" class="form-control" name="date">
						</div>
					</div>
				</div>
				<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
					<div class="form-group">
						<label>Submit:</label>
						<button class="btn bg-teal-400" id="btn_filter">Filter <i class="icon-arrow-right14 position-right"></i></button>
					</div>
				</div>
				<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
					<div class="form-group">
						<label>Cancel:</label>
						<button class="btn bg-indigo-300" id="btn_cancel_filter">Cancel <i class="icon-arrow-right14 position-right"></i></button>
					</div>
				</div>
			</div>
		</div>
		<table class="table datatable-basic table-bordered dataTable no-footer">
			<thead>
				<tr>
					<th width="5%">No.</th>
					<th width="20%">Name</th>
					<th width="20%">User</th>
					<th width="20%">Message</th>
					<th width="5%">Lat</th>
					<th width="5%">Lng</th>
					<th width="20%">Address</th>
					<th width="2%">Edit</th>
					<th width="3%">Delete</th>
				</tr>
			</thead>
			<tbody>
				<?php
					function cutLongString($in, $cntCut)
					{								
						$out = strlen($in) > $cntCut ? substr($in, 0, $cntCut)."..." : $in;
						return $out;						
					}
				?>
				<?php foreach ($restaurant as $key => $value): ?>
					<tr>
						<td width="5%"><?php echo $key + 1; ?></td>
						<td width="20%"><?php echo cutLongString($value['name'], 20); ?></td>
						<td width="20%"><?php echo $value['first_name'] . ' ' . $value['last_name']; ?></td>
						<td width="20%"><?php echo cutLongString($value['welcome_message'], 15); ?></td>
						<td width="5%"><?php echo cutLongString($value['lat'], 3); ?></td>
						<td width="5%"><?php echo cutLongString($value['lng'], 3); ?></td>
						<td width="20%"><?php echo cutLongString($value['address'], 20); ?></td>
						<td width="2%" class="edit"><i class="fa fa-edit" data-name="<?php echo $value['name']; ?>" data-id="<?php echo $value['id']; ?>"></i></td>
						<td width="3%" class="delete"><i class="fa fa-trash fa-remove" data-id="<?php echo $value['id']; ?>"></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	var tmpRestaurantData = '<?php echo json_encode($restaurant); ?>'
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/restaurant.js"></script>