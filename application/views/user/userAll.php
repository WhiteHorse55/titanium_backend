<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href=""><i class="icon-home2 position-left"></i>Users</a></li>
			<li class="active">all</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h5 class="panel-title">Users information</h5>
					<div class="heading-elements">
						<ul class="icons-list">
		            		<li><a data-action="collapse"></a></li>
		            		<li><a data-action="reload"></a></li>
		            		<li><a data-action="close"></a></li>
		            	</ul>
		        	</div>
				</div>
				<div class="panel-body">
				</div>
				<table class="display" id = "clients-all-table">
					<thead>
						<tr>
							<th >No.</th>
							<th >Email</th>
							<th >Password</th>
							<th >Firstname</th>
							<th >Lastname</th>
							<th >Phonenumber</th>
							<th >Timestamp</th>
							<th >Delete</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/table_userAll.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">