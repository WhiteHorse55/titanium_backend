<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Category</span> - Booking</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href=""><i class="icon-home2 position-left"></i> Category</a></li>
			<li class="active">Booking</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-3">
			<?php if(isset($booking)): ?>
				<form name="form_booking" class="form_booking form-horizontal" action="<?php echo base_url(); ?>v1/booking/update" method="post">
		            <div class="panel panel-flat">
						<div class="panel-heading">
			                <h6 class="panel-title">Add / Update bookings</h6>
						</div>

		                <div class="panel-body">
		                	<div class="form-group">
		                		<input type="hidden" name="id" value="<?php echo $booking['id']; ?>">
								<label class="col-lg-2 col-md-2 col-sm-2 control-label" for="user_id">User:</label>
								<div class="col-lg-10 col-md-10 col-sm-10">
									<select data-placeholder="Select user" class="select" name="user_id">
										<option></option>
										<optgroup label="Registered Users">
											<?php foreach ($users as $key => $value): 
												$selected = "";
												if($value['id'] == $booking['user_id']) $selected = " selected";
											?>
											<option value="<?php echo $value['id']; ?>"<?php echo $selected;?>><?php echo $value['first_name'] . ' ' . $value['last_name']; ?></option>
											<?php endforeach; ?>
										</optgroup>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 col-md-2 col-sm-2 control-label" for="menu_id">Menu:</label>
								<div class="col-lg-10 col-md-10 col-sm-10">
									<select data-placeholder="Select menu" class="select" name="menu_id">
										<option></option>
										<optgroup label="Registered Menus">
											<?php foreach ($menus as $key => $value): 
												$selected = "";
												if($value['id'] == $booking['menu_id']) $selected = " selected";
											?>
											<option value="<?php echo $value['id']; ?>"<?php echo $selected;?>><?php echo $value['title']; ?></option>
											<?php endforeach; ?>
										</optgroup>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 col-md-2 col-sm-2 control-label" for="takeaway_plate">Takeaway Plate:</label>
								<div class="col-lg-10 col-md-10 col-sm-10">
									<input type="number" class="form-control form-input" name="takeaway_plate" placeholder="Input takeaway plate" value="<?php echo $booking['takeaway_plate']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 col-md-2 col-sm-2 control-label" for="dine_plate">Dine Plate:</label>
								<div class="col-lg-10 col-md-10 col-sm-10">
									<input type="number" class="form-control form-input" name="dine_plate" placeholder="Input dine plate" value="<?php echo $booking['dine_plate']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 col-md-2 col-sm-2 control-label" for="booking_date">Date:</label>
								<div class="col-lg-10 col-md-10 col-sm-10">
									<div class="input-group date" data-provide="datepicker">
										<div class="input-group-addon">
											<span class="glyphicon glyphicon-th"></span>
										</div>
										<input type="text" class="form-control form-input" name="booking_date" value="<?php echo $booking['booking_date']; ?>">
									</div>
									<!-- <div class='input-group date' id='datetimepicker1'>
										<input type='text' class="form-control form-input" name="booking_date" value="<?php echo $booking['booking_date']; ?>"/>
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div> -->
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 col-md-2 col-sm-2 control-label" for="status">status:</label>
								<div class="col-lg-10 col-md-10 col-sm-10">
									<div class="checkbox">
										<?php if($booking['status'] == 'on'): ?>
											<input type="checkbox" class="styled" name="status" checked="">
										<?php elseif($booking['status'] == 'off'): ?>
											<input type="checkbox" class="styled" name="status">
										<?php endif; ?>
									</div>
								</div>
							</div>
							<p class="status_error_message"></p>
							<div class="text-right">
								<input type="submit" class="btn btn-info btn_action">
							</div>
						</div>
					</div>
				</form>
			<?php else: ?>
				<form name="form_booking" class="form_booking form-horizontal" action="<?php echo base_url(); ?>v1/booking/add" method="post">
		            <div class="panel panel-flat">
						<div class="panel-heading">
			                <h6 class="panel-title">Add / Update bookings</h6>
						</div>

		                <div class="panel-body">
		                	<div class="form-group">
								<label class="col-lg-2 col-md-2 col-sm-2 control-label" for="user_id">User:</label>
								<div class="col-lg-10 col-md-10 col-sm-10">
									<select data-placeholder="Select user" class="select" name="user_id">
										<option></option>
										<optgroup label="Registered Users">
											<?php foreach ($users as $key => $value): ?>
												<option value="<?php echo $value['id']; ?>"><?php echo $value['first_name'] . ' ' . $value['last_name']; ?></option>
											<?php endforeach; ?>
										</optgroup>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 col-md-2 col-sm-2 control-label" for="menu_id">Menu:</label>
								<div class="col-lg-10 col-md-10 col-sm-10">
									<select data-placeholder="Select menu" class="select" name="menu_id">
										<option></option>
										<optgroup label="Registered Menus">
											<?php foreach ($menus as $key => $value): ?>
												<option value="<?php echo $value['id']; ?>"><?php echo $value['title']; ?></option>
											<?php endforeach; ?>
										</optgroup>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 col-md-2 col-sm-2 control-label" for="takeaway_plate">Takeaway Plate:</label>
								<div class="col-lg-10 col-md-10 col-sm-10">
									<input type="number" class="form-control form-input" name="takeaway_plate" placeholder="Input takeaway plate">
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 col-md-2 col-sm-2 control-label" for="dine_plate">Dine Plate:</label>
								<div class="col-lg-10 col-md-10 col-sm-10">
									<input type="number" class="form-control form-input" name="dine_plate" placeholder="Input dine plate">
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 col-md-2 col-sm-2 control-label" for="booking_date">Date:</label>
								<div class="col-lg-10 col-md-10 col-sm-10">
									<div class="input-group date" data-provide="datepicker">
										<div class="input-group-addon">
											<span class="glyphicon glyphicon-th"></span>
										</div>
										<input type="text" class="form-control form-input" name="booking_date">
									</div>
									<!-- <div class='input-group date' id='datetimepicker1'>
										<input type='text' class="form-control form-input" name="booking_date"/>
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div> -->
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 col-md-2 col-sm-2 control-label" for="status">status:</label>
								<div class="col-lg-10 col-md-10 col-sm-10">
									<div class="checkbox">
										<input type="checkbox" class="styled" name="status">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
									<p class="status_error_message"></p>
								</div>
							</div>
							<div class="text-right">
								<input type="submit" class="btn btn-info btn_action">
							</div>
						</div>
					</div>
				</form>
			<?php endif; ?>
		</div>
	</div>
</div>
<style type="text/css">
	.StripeElement {
		background-color: white;
		height: 40px;
		padding: 10px 12px;
		border-radius: 4px;
		border: 1px solid transparent;
		box-shadow: 0 1px 3px 0 #e6ebf1;
		-webkit-transition: box-shadow 150ms ease;
		transition: box-shadow 150ms ease;
	}

	.StripeElement--focus {
		box-shadow: 0 1px 3px 0 #cfd7df;
	}

	.StripeElement--invalid {
		border-color: #fa755a;
	}

	.StripeElement--webkit-autofill {
		background-color: #fefde5 !important;
	}
</style>
<script src="https://js.stripe.com/v3/"></script>
<form action="/charge" method="post" id="payment-form">
	<div class="form-row">
		<label for="card-element"> Credit or debit card	</label>
		<div id="card-element"> </div>
		<div id="card-errors" role="alert"></div>
	</div>
	<button>Submit Payment</button>
</form>
<script type="text/javascript">
	var action = "<?php echo $action; ?>"
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/booking_form.js"></script>