<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Category</span> - Booking</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href=""><i class="icon-home2 position-left"></i> Category</a></li>
			<li class="active">Booking</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h5 class="panel-title">Booking tables</h5>
					<div class="heading-elements">
						<ul class="icons-list">
		            		<li><a data-action="collapse"></a></li>
		            		<li><a data-action="reload"></a></li>
		            		<li><a data-action="close"></a></li>
		            	</ul>
		        	</div>
				</div>
				<div class="panel-body">
					
				</div>
				<table class="table datatable-basic table-bordered dataTable no-footer">
					<thead>
						<tr>
							<th>No.</th>
							<th>User</th>
							<th>Menu</th>
							<th>Takeaway</th>
							<th>Dine</th>
							<th>Status</th>
							<th>Date</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($bookings as $key => $value): ?>
							<tr>
								<td><?php echo $key + 1; ?></td>
								<td><?php echo $value['first_name'].' '.$value['last_name']; ?></td>
								<td><?php echo $value['title']; ?></td>
								<td><?php echo $value['takeaway_plate']; ?></td>
								<td><?php echo $value['dine_plate']; ?></td>								
								<td>
									<?php if($value['status'] == 'on'): ?>
										<span class="label bg-blue">Complete</span>
									<?php elseif($value['status'] == 'off'): ?>
										<span class="label bg-success-400">Pending</span>
									<?php endif; ?>
								</td>
								<td><?php echo $value['booking_date']; ?></td>
								<td class="edit"><i class="fa fa-edit" data-id="<?php echo $value['id']; ?>"></i></td>
								<td class="delete"><i class="fa fa-trash fa-remove" data-id="<?php echo $value['id']; ?>"></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/booking.js"></script>