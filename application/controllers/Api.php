<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Twilio\Rest\Client;

require_once "vendor/autoload.php";
class Api extends CI_Controller {

	public function __construct() {
        parent::__construct();

        $this->load->model('model_user');
	}
	

    public function login_email(){
      
		$email = $this->input->get_post('email');
		$password = $this->input->get_post('password');

        $result = $this->model_user->getByEmail($email);
        
		if(is_array($result) && !empty($result)) {
            if($result['password'] == $password){
                $result['result'] = '1';
                $result['message'] = "Login Success";
            }
            else{
                $result['result'] = '0';
                $result['message'] = 'Password incorrect';
            }
		} else {
            $result['result'] = '0';
            $result['message'] = 'Email does not exist';
		}
        echo json_encode($result);
    }
    
    public function login_phonenumber(){
      
		$phonenumber = $this->input->get_post('phonenumber');
		$password = $this->input->get_post('password');

        $result = $this->model_user->getByPhonenumber($phonenumber);
        
		if(is_array($result) && !empty($result)) {
            if($result['password'] == $password){
                $result['result'] = '1';
                $result['message'] = "Login Success";
            }
            else{
                $result['result'] = '0';
                $result['message'] = 'Password incorrect';
            }
		} else {
            $result['result'] = '0';
            $result['message'] = 'Phonenumber does not exist';
		}
        echo json_encode($result);
    }

    public function isNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }

    public function login_social(){
      
        $email = $this->input->get_post('email');
        $firstname = $this->input->get_post('firstname');
        $lastname = $this->input->get_post('lastname');

        if($this->isNullOrEmptyString($email)){
            $result['result'] = '0';
            $result['message'] = "Email Null";
            echo json_encode($result);            return;
        }
        
        if($this->isNullOrEmptyString($firstname)){
            $result['result'] = '0';
            $result['message'] = "firstname Null";
            echo json_encode($result);            return;
        }

        if($this->isNullOrEmptyString($lastname)){
            $result['result'] = '0';
            $result['message'] = "lastname Null";
            echo json_encode($result);            return;
        }

        $result = $this->model_user->getByEmail($email);
        
		if(is_array($result) && !empty($result)) {
            $result['result'] = '1';
            $result['message'] = "Login Success";
		} else {
            $result = array(
                'email' =>$email, 
                'password' => 'SOCIAL',
                'firstname' => $firstname,
                'lastname' => $lastname,
                'phonenumber' => '',
                'country' => '',
                'city' => '',
                'state' => ''
            );
            $this->model_user->saveData($result);
            $result['result'] = '2';
            $result['message'] = 'Created new user';
		}
        echo json_encode($result);
    }

    public function reset_password(){
        $email = $this->input->get_post('email');
        $password = $this->input->get_post('password');

        $result = $this->model_user->getByEmail($email);
        
		if(is_array($result) && !empty($result)) {
            $result['password'] = $password;
            $this->model_user->saveData($result);
            $data['result'] = '1';
            $data['message'] = 'Password Updated';
		} else {
            $data['result'] = '0';
            $data['message'] = 'Email does not exist';
		}
        echo json_encode($data);
    }

    public function verify_phonenumber(){
		$in_number = $this->input->get_post('phonenumber');
		$number = "+".$this->input->get_post('phonenumber');
		$value = rand(1000, 9999);

		$content = "TitaniumPay verify code: ".$value;

		$sid    = "ACb3febf6a2a513828f3b1489d5a6215a2"; 
		$token  = "afe64d136e11c4fea3522572f486115a"; 
		$twilio = new Client($sid, $token); 
		 
		$message = $twilio->messages 
						  ->create($number, // to 
								   array( 
									   "from" => "+14704109144",       
									   "body" => $content 
								   ) 
						  ); 
		
		$array = array('result' => '1', 'message' => 'verify code sent.', 'number' => $in_number, 'code' => $value."");
		echo json_encode($array);
    }
    
    public function verify_email(){
		// $in_number = $this->input->get_post('phonenumber');
		// $number = "+".$this->input->get_post('phonenumber');
		// $value = rand(1000, 9999);

		// $content = "TitaniumPay verify code: ".$value;

		// $sid    = "ACb3febf6a2a513828f3b1489d5a6215a2"; 
		// $token  = "afe64d136e11c4fea3522572f486115a"; 
		// $twilio = new Client($sid, $token); 
		 
		// $message = $twilio->messages 
		// 				  ->create($number, // to 
		// 						   array( 
		// 							   "from" => "+14704109144",       
		// 							   "body" => $content 
		// 						   ) 
		// 				  ); 
		
		// $array = array('result' => '1', 'message' => 'verify code sent.', 'number' => $in_number, 'code' => $value."");
		// echo json_encode($array);
	}

    public function signup(){
        $email = $this->input->get_post('email');
        $password = $this->input->get_post('password');
        $firstname = $this->input->get_post('firstname');
        $lastname = $this->input->get_post('lastname');
        $phonenumber = $this->input->get_post('phonenumber');
        $country = $this->input->get_post('country');
        $city = $this->input->get_post('city');
        $state = $this->input->get_post('state');

        $result = $this->model_user->getByEmail($email);
        
		if(is_array($result) && !empty($result)) {
            $data['result'] = '0';
            $data['message'] = "Email already exist";
            echo json_encode($data);
            return;
        }
        
        $result = $this->model_user->getByPhonenumber($phonenumber);
        
		if(is_array($result) && !empty($result)) {
            $data['result'] = '0';
            $data['message'] = "Phonenumber already exist";
            echo json_encode($data);
            return;
        }

        $data = array(
            'email' =>$email, 
            'password' => $password,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'phonenumber' => $phonenumber,
            'country' => $country,
            'city' => $city,
            'state' => $state
        );

        $this->model_user->create($data);

        $data = array();
        $data['result'] = '1';
        $data['message'] = 'Created new user';
        echo json_encode($data);
    }
    
    public function delete(){
        $id = $this->input->get_post('id');

        $this->model_user->delete_item($id);

        $data = array();
        $data['result'] = '1';
        $data['message'] = 'Deleted the user' . $id;
        echo json_encode($data);
    }
}
