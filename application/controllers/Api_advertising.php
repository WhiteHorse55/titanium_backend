<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_advertising extends CI_Controller {
    public function __construct() {
        parent::__construct();
        
        $this->load->model('model_advertising');
    }

    public function index(){
        // echo "hello";
    }

    public function create(){
        $title      =   $this->input->get_post('title');
        $company    =   $this->input->get_post('company');
        $type       =   $this->input->get_post('type');
        $image1     =   $this->input->get_post('image1');
        $image2     =   $this->input->get_post('image2');
        $image3     =   $this->input->get_post('image3');
        $video      =   $this->input->get_post('video');


        $data = array(
            'title'     =>  $title, 
            'company'   =>  $company,
            'type'      =>  $type,
            'image1'    =>  $image1,
            'image2'    =>  $image2,
            'image3'    =>  $image3,
            'video'     =>  $video,
        );

        $this->model_advertising->create($data);

        $data = array();
        $data['result'] = '1';
        $data['message'] = 'Created new advertising';
        echo json_encode($data);
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function upload_file(){       
        
        $date = date('YmdHis');
        $filename = "file".$date.generateRandomString(5);

        if (isset($_FILES['file'])) {

            $config = array(
                'file_name' => $filename,
                'upload_path' => "./upload/advertising",
                'allowed_types' => "gif|jpg|png|PNG|jpeg|avi|mpeg|mp3|mp4|3gp",
                'overwrite' => TRUE,
                'max_size' => "300000000", // Can be set to particular file size , here it is 20 MB(2048 Kb)
                );

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $upload_data = $this->upload->data();

                $data['result'] = '1';
                $data['message'] =  "upload/advertising/".$upload_data['file_name'];
            } else {
                $data['result'] = '0';
                $data['message'] = $this->upload->display_errors('', '');
            }
        }
        else{
            $data['result'] = '0';
            $data['message'] = "file not exist";
        }
        echo json_encode($data);
    }
    
    // API
    public function get_page(){

        $start = $this->input->get_post("start");
        $length = $this->input->get_post("length");
        
        $filteropt = array(
        	"start"=>$start,
        	"length"=>$length
        );

        $data = $this->model_advertising->getPage($filteropt);

        $senddata['result'] = '1';
        $senddata['message'] = $data;

        echo json_encode($senddata);
    }

    // API
    public function getSearchData(){

    	$order_i = $this->input->get_post("order[0][column]");
        $order_field = $this->input->get_post("columns[".$order_i. "][data]");
        $order_dir = $this->input->get_post("order[0][dir]");
        $length = $this->input->get_post("length");
        $start = $this->input->get_post("start");
        $searchval = $this->input->get_post("search[value]");
        $draw = $this->input->get_post("draw");
        
        $filteropt = array(
        	"order_i"=>$order_i,
        	"order_field"=>$order_field,
        	"order_dir"=>$order_dir,
        	"length"=>$length,
        	"start"=>$start,
        	"search"=>$searchval,
        );

        $data = $this->model_advertising->getAll($filteropt);

        $senddata = array(
		  "draw"=> $draw,
		  "recordsTotal"=> $this->model_advertising->getTotalCount($filteropt),
		  "iTotalDisplayRecords"=> $this->model_advertising->getTotalCount($filteropt),
		  "recordsFiltered"=> count($data),
		  "data"=> $data
		);

        echo json_encode($senddata);
    }

    public function delete(){
        $id = $this->input->get_post('id');

        $this->model_advertising->delete_item($id);

        $data = array();
        $data['result'] = '1';
        $data['message'] = 'Deleted the user' . $id;
        echo json_encode($data);
    }
}
?>