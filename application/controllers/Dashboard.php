<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	protected $admin_user = array();

	public function __construct() {	
		parent::__construct();
		$this->load->model('user_model');
		if(!$this->session->userdata('admin_user')){
            redirect('/');
        } else {
            $this->admin_user = $this->session->userdata('admin_user');
        }
	}

	public function __prefix($page, $data = array()) {
		$this->load->view('layout/header', $data);
		$this->load->view($page, $data);
		$this->load->view('layout/footer', $data);
	}

	public function index()
	{
		$data = array(
			'title' => SITE_TITLE,
			'page' => SIDEBAR_DASHBOARD,
			'admin_user' => $this->admin_user
		);
		$this->__prefix('index', $data);
	}
}
