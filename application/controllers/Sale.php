<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Twilio\Rest\Client;

class Sale extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	protected $site_title = SITE_TITLE;
	protected $admin_user = array();

	public function __construct() {	
		parent::__construct();

		$this->load->model('sale_model');
		$this->load->library('excel');

		if(!$this->session->userdata('admin_user')){
            redirect('/');
        } else {
            $this->admin_user = $this->session->userdata('admin_user');
        }
	}

	public function __prefix($page, $data = array()) {
		$this->load->view('layout/header', $data);
		$this->load->view($page, $data);
		$this->load->view('layout/footer', $data);
	}

	public function index()
	{
		$data = array(
			'title' => $this->site_title,
			'page' => SIDEBAR_SALEREQUEST,
			'admin_user' => $this->admin_user
		);
		$data['sale_request'] = $this->sale_model->getSaleRequests();
		$this->__prefix('sale/request', $data);
	}
	
	public function go_request()
	{
		$data = array(
			'title' => $this->site_title,
			'page' => SIDEBAR_SALEREQUEST,
			'admin_user' => $this->admin_user
		);
		$data['requests'] = $this->sale_model->getSaleRequests();
		$this->__prefix('sale/request', $data);
	}


	public function request_detail($id) {

		$data = array(
			'title' => SITE_TITLE,
			'page' => SIDEBAR_SALEREQUEST_EDIT,
			'admin_user' => $this->admin_user,
			'request' => $this->sale_model->getSaleReqeustById($id),
			'action' => 'edit'
		);


		$this->__prefix('sale/request_form', $data);
	}

	public function update_request()  {

		$data = array(
			'useremail' => $this->input->get_post('useremail'),
			'source_request' => $this->input->get_post('source_request'),
			'price' => $this->input->get_post('price'),
			'vincode' => $this->input->get_post('vincode'),
			'zipcode' => $this->input->get_post('zipcode'),
			'mileage' => $this->input->get_post('mileage'),
			'year' => $this->input->get_post('year'),
			'make' => $this->input->get_post('make'),
			'model' => $this->input->get_post('model'),
			'trimlevel' => $this->input->get_post('trimlevel'),
			'userphone' => $this->input->get_post('userphone'),
			'note' => $this->input->get_post('note')
		);	

		$id = $this->input->get_post('id');
		$result_flag = $this->sale_model->updateSaleRequest($data, $id);

		if($result_flag){
			echo json_encode(array('type'=>'success'));
		}else{
			echo json_encode(array('type'=>'error'));
		}

	}

	public function delete_request()  {

		$id = $this->input->get_post('id');
		$result_flag = $this->sale_model->deleteSaleRequest($id);

		if($result_flag){
			echo json_encode(array('type'=>'success'));
		}else{
			echo json_encode(array('type'=>'error'));
		}
	}

	// create xlsx
    public function printSaleRequest($id) {

    	$upload_base = $this->config->item('upload')['upload_base'];
    	$upload_url = $this->config->item('upload')['upload_url'];


    	$data = $this->sale_model->getSaleReqeustById($id);

		// create file name
        $fileName = 'data-'.time().'.xlsx';  
		// load excel library
        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);


        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'User Email');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Phone Number');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Source Request');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Price');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Vin Code'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Zip Code'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Mileage'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Year'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Make'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Model'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Trim Level'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Note'); 

        // set Row
        $rowCount = 2;
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $data['useremail']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $data['userphone']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $data['source_request']);
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $data['price']);
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $data['vincode']); 
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $data['zipcode']); 
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $data['mileage']); 
        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $data['year']); 
        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $data['make']); 
        $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $data['model']); 
        $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $data['trimlevel']);
        $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $data['note']);

        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($upload_base.$fileName);

		// download file
        header("Content-Type: application/vnd.ms-excel");
        redirect($upload_url.$fileName);        
    }

    public function sendEmail(){

    	$id = $this->input->get_post('id');
    	$data = $this->sale_model->getSaleReqeustById($id);
    	$toemail = $data['useremail'];

    	$this->load->library('email');
    	$this->email->from(HOST_EMAIL, 'carzili support team');
		$this->email->to($toemail);

		$this->email->subject('Your Sale Request');
		$message = '<h3>Thanks for your request</h3>';
		$message .= '<div>We review your request</div>';
		$message .= '<div class = "message-content" style = "diplay:table;">';
			$message .= '<div style = "diplay:table-row;"><span style = "diplay:table-cell;">Price: </span><span>'.$data['price'].'</span></div>';
			$message .= '<div style = "diplay:table-row;"><span style = "diplay:table-cell;">Vin Code: </span><span>'.$data['vincode'].'</span></div>';
			$message .= '<div style = "diplay:table-row;"><span style = "diplay:table-cell;">Zip Code: </span><span>'.$data['zipcode'].'</span></div>';
			$message .= '<div style = "diplay:table-row;"><span style = "diplay:table-cell;">Mileage: </span><span>'.$data['mileage'].'</span></div>';
			$message .= '<div style = "diplay:table-row;"><span style = "diplay:table-cell;">Year: </span><span>'.$data['year'].'</span></div>';
			$message .= '<div style = "diplay:table-row;"><span style = "diplay:table-cell;">Make: </span><span>'.$data['make'].'</span></div>';
			$message .= '<div style = "diplay:table-row;"><span style = "diplay:table-cell;">Model: </span><span>'.$data['model'].'</span></div>';
			$message .= '<div style = "diplay:table-row;"><span style = "diplay:table-cell;">TrimLevel: </span><span>'.$data['trimlevel'].'</span></div>';
		$message .= '</div>';
		$this->email->set_mailtype("html");

		$this->email->message($message);
		$this->email->send();

		echo $toemail;

    }

    public function getAllRequest(){

    	$order_i = $this->input->get_post("order[0][column]");
        $order_field = $this->input->get_post("columns[".$order_i. "][data]");
        $order_dir = $this->input->get_post("order[0][dir]");
        $length = $this->input->get_post("length");
        $start = $this->input->get_post("start");
        $searchval = $this->input->get_post("search[value]");

        $draw = $this->input->get_post("draw");
        
        $filteropt = array(
        	"order_i"=>$order_i,
        	"order_field"=>$order_field,
        	"order_dir"=>$order_dir,
        	"length"=>$length,
        	"start"=>$start,
        	"search"=>$searchval,
        );

        $data = $this->sale_model->getAllSaleRequests($filteropt);

        

        $senddata = array(
		  "draw"=> $draw,
		  "recordsTotal"=> $this->sale_model->getTotalCount($filteropt),
		  "iTotalDisplayRecords"=> $this->sale_model->getTotalCount($filteropt),
		  "recordsFiltered"=> count($data),
		  "data"=> $data
		);

        echo json_encode($senddata);

    }

    public function sendSMS(){
 		echo json_encode($this->sendProtectedSMS());
    }

    protected function sendProtectedSMS(){
 		$id = $this->input->get_post('id');
    	$reqinfo = $this->sale_model->getSaleReqeustById($id);

        $sid    = "AC2162b8f7903de336c0143b5c2e266b9b"; 
		$token  = "0638762f1cff8e7db148693fa0246fb0"; 
		$twilio = new Client($sid, $token); 

		$message = "Hi ".$reqinfo['username']."!, \n Here is your request detail.\n";
		$message .= "Price: ".$reqinfo['price']."\n";
		$message .= "Zip Code: ".$reqinfo['zipcode']."\n";
		$message .= "Vin Code: ".$reqinfo['vincode']."\n";
		$message .= "Year: ".$reqinfo['year']."\n";
		$message .= "Make: ".$reqinfo['make']."\n";
		$message .= "Model: ".$reqinfo['model']."\n";
		$message .= "Trim: ".$reqinfo['trimlevel']."\n";
		$message .= "Date: ".$reqinfo['date_time']."\n";
		$toPhone = $reqinfo['userphone'];
		 
		$message = $twilio->messages 
		                  ->create($toPhone, // to 
		                           array( 
		                               "from" => HOST_PHONE_NUMBER,       
		                               "body" => $message 
		                           ) 
		                  );
        return $message;
    }
    
}