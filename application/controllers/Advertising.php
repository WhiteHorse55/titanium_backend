<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Advertising extends CI_Controller {

	protected $site_title = SITE_TITLE;
	protected $admin_user = array();

	public function __construct() {	
		parent::__construct();

		$this->load->model('model_advertising');

		if(!$this->session->userdata('admin_user')){
            redirect('/');
        } else {
            $this->admin_user = $this->session->userdata('admin_user');
        }
	}

	public function __prefix($page, $data = array()) {
		$this->load->view('layout/header', $data);
		$this->load->view($page, $data);
		$this->load->view('layout/footer', $data);
	}

	public function index()
	{
		$data = array(
			'title' => $this->site_title,
			'page' => 'advertising_all',
			'admin_user' => $this->admin_user
		);
		$this->__prefix('advertising/advertisingAll', $data);
    }
    
	public function all()
	{
		$data = array(
			'title' => $this->site_title,
			'page' => 'advertising_all',
			'admin_user' => $this->admin_user
		);
		$this->__prefix('advertising/advertisingAll', $data);
    }
}