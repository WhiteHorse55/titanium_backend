<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Upload_controller extends CI_Controller {
    public function __construct() {
        parent::__construct();
    }

    public function index(){
       $this->load->view('fileupload/custom_view', array('error' => ' ' ));
    }

    public function custom_view(){
       $this->load->view('fileupload/custom_view', array('error' => ' ' ));
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function do_upload(){

        $date = date('YmdHis');
        $filename = "file".$date.generateRandomString(5);

        $config = array(
        'file_name' => $filename,
        'upload_path' => "./upload/",
        'allowed_types' => "gif|jpg|png|jpeg|avi|mpeg|mp3|mp4|3gp",
        'overwrite' => TRUE,
        'max_size' => "300000000", // Can be set to particular file size , here it is 20 MB(2048 Kb)
        // 'max_height' => "768",
        // 'max_width' => "1024"
        );
        $this->load->library('upload', $config);
        if($this->upload->do_upload())
        {
            $data = array('upload_data' => $this->upload->data());
            $this->load->view('fileupload/upload_success',$data);
        }
        else
        {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('fileupload/custom_view', $error);
        }
        }
    }
?>