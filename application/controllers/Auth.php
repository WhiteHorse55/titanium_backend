<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Auth extends CI_Controller {

	/**
	 * Index Page for this controller.
	 */

	protected $logged;
	protected $site_title = SITE_TITLE;
	protected $admin_user = array();

	public function __construct() {	
		parent::__construct();
		$this->load->model('model_admin');
	}

	public function index()
	{
		$logged = $this->session->userdata('admin_user');
        if (empty($logged)) { $this->logged = 0; } else { $this->logged = 1;}
    
		if($this->logged == 0) { 
			$data = array(
				'title' => SITE_TITLE
			);
			$this->load->view('auth/login', $data); 
		}
		else {
		    
			$data = array(
				'title' => SITE_TITLE,
				'page' => 'user_all',
				'admin_user' => $this->session->userdata('admin_user')
			);
			
			$this->load->view('layout/header', $data);
			$this->load->view('user/userAll', $data);
			$this->load->view('layout/footer', $data);
		}		
	}

	public function create() {
		$data = array(
			'title' => $this->site_title
		);
		$this->load->view('auth/signup', $data);
	}

	public function store() {
		$data = array(
			'username' => $this->input->get_post('username'),
			'password' => $this->input->get_post('password'),
			'created' => date('Y-m-d')
		);
		$isUserExist = $this->admin_model->isUserExist($data['username']);
		if($isUserExist) {
			$data['msg'] = MSG_SIGNUP_FAIL_DUPLICATE;
			echo $data['msg'];
		} else {
			$this->admin_model->addUser($data);
			$data['msg'] = MSG_SIGNUP_SUCCESS;
			echo $data['msg'];
		}
	}

	public function go_login() {
		redirect('/');
	}

	public function login() {
		
		$username = $this->input->get_post('username');
		$password = $this->input->get_post('password');
		
		$user = $this->model_admin->getByName($username);
		
		if(is_array($user) && !empty($user)) {
			if($password == $user['password']){
				$this->session->set_userdata('admin_user', $user);	
				echo "success";		
			}
			else
				echo "password incorrect";
		} else {
			echo "username not exist";
		}
	}

	public function logout() {
		$this->session->unset_userdata('admin_user');
		$this->session->sess_destroy();
		redirect('/', 'refresh');
	}
}
