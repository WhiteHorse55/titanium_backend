<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	protected $site_title = SITE_TITLE;
	protected $admin_user = array();

	public function __construct() {	
		parent::__construct();

		$this->load->model('model_user');

		if(!$this->session->userdata('admin_user')){
            redirect('/');
        } else {
            $this->admin_user = $this->session->userdata('admin_user');
        }
	}

	public function __prefix($page, $data = array()) {
		$this->load->view('layout/header', $data);
		$this->load->view($page, $data);
		$this->load->view('layout/footer', $data);
	}

	public function index()
	{
		$data = array(
			'title' => $this->site_title,
			'page' => 'user_all',
			'admin_user' => $this->admin_user
		);
		$this->__prefix('user/userAll', $data);
    }
    
	public function all()
	{
		$data = array(
			'title' => $this->site_title,
			'page' => 'user_all',
			'admin_user' => $this->admin_user
		);
		$this->__prefix('user/userAll', $data);
    }

    // API
    public function getSearchData(){

    	$order_i = $this->input->get_post("order[0][column]");
        $order_field = $this->input->get_post("columns[".$order_i. "][data]");
        $order_dir = $this->input->get_post("order[0][dir]");
        $length = $this->input->get_post("length");
        $start = $this->input->get_post("start");
        $searchval = $this->input->get_post("search[value]");
        $draw = $this->input->get_post("draw");
        
        $filteropt = array(
        	"order_i"=>$order_i,
        	"order_field"=>$order_field,
        	"order_dir"=>$order_dir,
        	"length"=>$length,
        	"start"=>$start,
        	"search"=>$searchval,
        );

        $data = $this->model_user->getAll($filteropt);

        $senddata = array(
		  "draw"=> $draw,
		  "recordsTotal"=> $this->model_user->getTotalCount($filteropt),
		  "iTotalDisplayRecords"=> $this->model_user->getTotalCount($filteropt),
		  "recordsFiltered"=> count($data),
		  "data"=> $data
		);

        echo json_encode($senddata);
    }
}